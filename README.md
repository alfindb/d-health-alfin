## Installation
- Install xampp terlebih dahulu, di rekomendasikan menggunakan PHP 7.3
- Buka command line pada folder (rekomendasi menggunakan gitbash)
- ketikkan composer install, tunggu hingga proses selesai
- copy file .env.example setelah itu paste di folder yang sama lalu rename file dengan nama .env
- pada file .env, atur database name sesuai dengan nama database yang telah anda buat, dan isi username dengan passwordnya berdasarkan setting user mysql anda
- pada command line ketikkan php artisan key:generate
- lalu pada command line ketikkan php artisan serve
- jika akan melakukan migrasi cukup ketikkan php artisan migrate, lalu import data obat dan signa, yang terdapat pada folder /public/db/obatalkes_m.sql dan /public/db/signa_m.sql untuk signa
- jangan lupa untuk input data awal ketikkan php artisan db:seed
- jika tidak akan melakukan migrasi atau seeding, saya sudah menyediakan file.sql untuk di import yang terdapat di /public/db/db_dhealth.sql
- buka aplikasi dengan url defaultnya http://localhost:8000/

## Credentials
- Username : admin
- Password : admin123
