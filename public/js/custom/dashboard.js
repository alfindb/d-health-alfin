$(document).ready(function () {

	var base_url = $("#base_url").val();

    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        orientation: "bottom center",
        autoclose: true,
        todayHighlight: true,
    }); 

    onChangeYear();

    var year_filter = $('#year_filter').val()
    $('#year_filter').val(year_filter).trigger('change')

    function onChangeYear(){
        $('#year_filter').on('change', function(event) {
            year = $(this).val()

            $.ajax({
                url: base_url+'/dashboard/countUsersByYear',
                type: 'GET',
                data: {year: year},
            })
            .done(function( res ) {
                initChart(res.data.year_text, res.data.array_total)
            })
            .fail(function( res ) {
                console.log("error");
            });
            
        });
    }

    function initChart(year_text, data) {
		Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Jumlah User Per Bulan Tahun ' + year_text
            },
            xAxis: {
                categories: [
                    'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                allowDecimals: false,
                title: {
                    text: 'Orang'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    // '<td style="padding:0"><b>{point.y:.1f} orang</b></td></tr>',
                    '<td style="padding:0"><b>{point.y:.f} orang</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'User',
                data: data
            }]
        });
	}
});
