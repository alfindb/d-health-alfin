$(document).ready(function () {

	var base_url = $("#base_url").val();

    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        orientation: "bottom center",
        autoclose: true,
        todayHighlight: true,
    }); 

	initDatatable();
    FormValidation();

    function initDatatable() {
		if( $('#table').length > 0 ){
			$('#table').DataTable({
	            processing: true,
	            serverSide: true,
	            ajax: base_url+'/user/json',
	            "aoColumnDefs": [{
	                    "sWidth": "5%",
	                    "aTargets": [0],
	                    "className": "text-center"
	                }
	            ],
				"language": {
					"emptyTable": "Tidak Ada Data",
					"zeroRecords": "Data Tidak Ditemukan"
				},
	            columns: [
                    {
	                    data: 'DT_RowIndex',
	                    name: 'DT_RowIndex'
	                },
	                {
	                    data: 'full_name',
	                    name: 'full_name'
	                },
                    {
	                    data: 'username',
	                    name: 'username'
	                },
                    {
	                    data: 'email',
	                    name: 'email'
	                },
                    {
	                    data: 'role_name',
	                    name: 'role_name'
	                },
                    {
                        data: 'join_date',
                        name: 'join_date'
                    },
	                {
	                    data: 'action',
	                    name: 'action'
	                }
	            ]
	        });
		}
	}

    function FormValidation(){

        url_cek = base_url+"/user/checkIsExist/";

        if( tipe_form == 'add' ){
            url_cek_username = url_cek+"1"; // 1 untuk cek username
            url_cek_email = url_cek+"2"; // 2 untuk cek email
        }else{
            var id = $('#id').val();

            url_cek_username = url_cek+"1/"+id; // 1 untuk cek username
            url_cek_email = url_cek+"2/"+id; // 2 untuk cek email
        }

        $('#form').validate({
            rules: {
                full_name: {
                    required: true
                },
                username: {
                    required: true,
                    remote: {
                        url: url_cek_username,
                        type: "GET"
                    }
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: url_cek_email,
                        type: "GET"
                    }
                },
                password: {
                    required: true,
                    minlength: 5
                },
                repeat_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                role: {
                    required: true
                },
                join_date: {
                    required: true
                },
            },
            messages: {
                full_name: {
                    required: "Nama Lengkap tidak boleh kosong"
                },
                username: {
                    required: "Username tidak boleh kosong",
                    remote: "Username sudah terdaftar"
                },
                email: {
                    required: "Email tidak boleh kosong",
                    email: "Silakan masukkan alamat email yang valid",
                    remote: "Email sudah terdaftar"
                },
                password: {
                    required: "Password tidak boleh kosong",
                    minlength: "Password minimal 5 huruf"
                },
                repeat_password: {
                    required: "Ulangi Password tidak boleh kosong",
                    equalTo: "Password tidak sama"
                },
                role: {
                    required: "Role tidak boleh kosong"
                },
                join_date: {
                    required: "Tanggal Bergabung tidak boleh kosong"
                },
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
              error.addClass('invalid-feedback');
              element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
              $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
              $(element).removeClass('is-invalid');
            }
        });
    }
});
