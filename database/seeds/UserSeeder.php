<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create(array(
            'username' => "admin",
            'email' => "admin@gmail.com",
            'password' => bcrypt('admin123'),
            'role_id' => 1,
            'full_name' => "Admin",
            'join_date' => date('Y-m-d')
        ));
    }
}
