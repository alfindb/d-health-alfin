<?php

namespace App;

use DB;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'username', 'email', 'password', 'full_name', 'role_id', 'join_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'email_verified_at' => 'datetime',
    ];

    public static function findOne($where = array())
    {
        $find = DB::table('users')
            ->select('users.*', 'role.role_name as role_name')
            ->join('role','role.id','=','users.role_id', 'left')
            ->where($where)
            ->first();
        return $find;
    }

    public static function getAll($where = array())
    {
        $find = DB::table('users')
            ->select('users.*', 'role.role_name as role_name')
            ->join('role','role.id','=','users.role_id', 'left')
            ->where($where)
            ->get()->toArray();
        return $find;
    }

    public static function countUsersByMonth($year)
    {
        $results = DB::select( DB::raw("
            SELECT count(id) as total, MONTH(users.join_date) as month
            FROM users
            WHERE YEAR(users.join_date) = $year
            GROUP BY MONTH(users.join_date) ") );

        return $results;
    }

    function role(){
        return $this->hasOne(Role::class,'id','role_id');
    }

}
