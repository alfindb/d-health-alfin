<?php

namespace App;

use DB;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class StokTemporary extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'obatalkes_id', 'stok', 'created_by'
    ];

    protected $table = 'stok_temporary';

    public static function findOne($where = array())
    {
        $find = DB::table('stok_temporary')
            ->select('stok_temporary.*')
            ->where($where)
            ->first();
        return $find;
    }

    public static function getAll($where = array())
    {
        $find = DB::table('stok_temporary')
            ->select('stok_temporary.*')
            ->where($where)
            ->get()->toArray();
        return $find;
    }

    public static function getOneSum($where = array())
    {
        $find = DB::table('stok_temporary')
            ->select(DB::raw('SUM(stok) as stok, obatalkes_id'))
            ->where($where)
            ->groupBy('stok_temporary.obatalkes_id')
            ->get()->first();
        return $find;
    }

}
