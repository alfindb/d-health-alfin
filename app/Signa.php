<?php

namespace App;

use DB;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Signa extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        
    ];

    protected $table = 'signa_m';

    public static function findOne($where = array())
    {
        $find = DB::table('signa_m')
            ->select('signa_m.*')
            ->where($where)
            ->first();
        return $find;
    }

    public static function getAll($where = array())
    {
        $find = DB::table('signa_m')
            ->select('signa_m.*')
            ->where($where)
            ->get()->toArray();
        return $find;
    }

}
