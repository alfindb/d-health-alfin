<?php

namespace App\Exports;

use App\Agama;
use App\Pendidikan;
use App\Pekerjaan;
use App\Provinsi;
use App\KabupatenKota;
use App\Kecamatan;
use App\KelurahanDesa;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Events\BeforeSheet;

class KKExport implements WithColumnWidths, WithHeadings, ShouldAutoSize, WithStyles, WithEvents, WithTitle, WithColumnFormatting

{
    /**
     * @return \Illuminate\Support\Collection
     */
    //function header in excel
    public function registerEvents(): array
    {
        return [
            AfterSheet::class=>function (AfterSheet $event) {
                $event->sheet->getProtection()->setPassword('kbyn_grp_smartvillage123')->setSheet(true);
                $sheet = $event->getSheet()->getDelegate();
                $sheet->getParent()->getDefaultStyle()->getProtection()->setLocked(false);
                
                $event->sheet->mergeCells('A2:P2');
                $event->sheet->setCellValue('A2', '*) Catatan: Baris pertama merupakan contoh, silahkan isi data berikutnya sesuai dengan contoh pada baris pertama');

                // Contoh Input
                $event->sheet->setCellValue('A3', '3213042604170005 (terdiri dari 16 angka)');
                $event->sheet->setCellValue('B3', 'DSN MEKARSARI');
                $event->sheet->setCellValue('C3', '41281 (terdiri dari 5 angka)');
                $event->sheet->setCellValue('D3', '001');
                $event->sheet->setCellValue('E3', '001');
                $event->sheet->setCellValue('F3', '3213041403714034 (terdiri dari 16 angka)');
                $event->sheet->setCellValue('G3', 'Yudi Eka Saputra');
                $event->sheet->setCellValue('H3', 'Jakarta');
                $event->sheet->setCellValue('I3', '14/03/1970 (tanggal/bulan/tahun)');
                $event->sheet->setCellValue('J3', 'Laki-laki');
                $event->sheet->setCellValue('K3', 'Islam');
                $event->sheet->setCellValue('L3', 'SLTA/SEDERAJAT');
                $event->sheet->setCellValue('M3', 'WIRASWASTA');
                $event->sheet->setCellValue('N3', 'Kawin');
                $event->sheet->setCellValue('O3', 'Agus');
                $event->sheet->setCellValue('P3', 'Cicih');


                // Set Protection Input
                $event->sheet->getStyle('A2:P3')->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_PROTECTED);

                // set background color Input
                $event->sheet->getStyle('A2:P3')->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('f2f2f2');

                // set background color Header (Input Data)
                $event->sheet->getStyle('A1:P1')->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('cccccc');

                // set background color Header (Agama, Pendidikan, Pekerjaan)
                $event->sheet->getStyle('R1:T1')->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('cccccc');
                $event->sheet->getStyle('A1:P103')->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                ]);
                // set Border (Input Data)
                $event->sheet->getStyle('A1:P103')->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                ]);

                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);

                // get layout counts (add 1 to rows for heading row)
                $row_count = 103;

                // set dropdown column
                //  jenis kelamin
                $jk_column = 'J';

                // agama
                $agama_column = 'K';

                // pendidikan
                $pendidikan_column = 'L';

                // pekerjaan
                $pekerjaan_column = 'M';

                // set dropdown options
                $jk_options = [
                    'Laki-laki',
                    'Perempuan',
                ];

                $status_perkawinan_column = 'N';

                $status_perkawinan_options = [
                    'Kawin',
                    'Belum Kawin',
                    'Cerai Hidup',
                    'Cerai Mati'
                ];

                // agama 
                $agama_list = Agama::all();
                foreach ($agama_list as $index_agama => $nama_agama) {
                    $no_agama = $index_agama + 2;
                    $event->sheet->SetCellValue('R' . $no_agama, $nama_agama->nama);
                }
                $no_akhir_agama = count($agama_list) + 1;

                // Set Protection
                $event->sheet->getStyle('R2:R' . $no_akhir_agama)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_PROTECTED);

                // set background color Header (Agama)
                $event->sheet->getStyle('R2:R' . $no_akhir_agama)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('f2f2f2');

                // set Border (Agama)
                $event->sheet->getStyle('R1:R' . $no_akhir_agama)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                ]);

                // pendidikan 
                $pendidikan_list = Pendidikan::all();
                foreach ($pendidikan_list as $index_pendidikan => $nama_pendidikan) {
                    $no_pendidikan = $index_pendidikan + 2;
                    $event->sheet->SetCellValue('S' . $no_pendidikan, $nama_pendidikan->nama);
                }
                $no_akhir_pendidikan = count($pendidikan_list) + 1;

                // Set Protection
                $event->sheet->getStyle('S2:S' . $no_akhir_pendidikan)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_PROTECTED);

                // set background color Header (Pendidikan)
                $event->sheet->getStyle('S2:S' . $no_akhir_pendidikan)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('f2f2f2');

                // set Border (Pendidikan)
                $event->sheet->getStyle('S1:S' . $no_akhir_pendidikan)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                ]);

                // pekerjaan 
                $pekerjaan_list = Pekerjaan::all();
                foreach ($pekerjaan_list as $index_pekerjaan => $nama_pekerjaan) {
                    $no_pekerjaan = $index_pekerjaan + 2;
                    $event->sheet->SetCellValue('T' . $no_pekerjaan, $nama_pekerjaan->nama);
                }
                $no_akhir_pekerjaan = count($pekerjaan_list) + 1;

                // Set Protection
                $event->sheet->getStyle('T2:T' . $no_akhir_pekerjaan)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_PROTECTED);

                // set background color Header (Pekerjaan)
                $event->sheet->getStyle('T2:T' . $no_akhir_pekerjaan)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('f2f2f2');

                // set Border (Pekerjaan)
                $event->sheet->getStyle('T1:T' . $no_akhir_pekerjaan)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                ]);

                // set dropdown list for first data row
                // jenis kelamin
                $jk = $event->sheet->getCell("{$jk_column}2")->getDataValidation();
                $jk->setType(DataValidation::TYPE_LIST);
                $jk->setErrorStyle(DataValidation::STYLE_INFORMATION);
                $jk->setAllowBlank(false);
                $jk->setShowInputMessage(true);
                $jk->setShowErrorMessage(true);
                $jk->setShowDropDown(true);
                $jk->setErrorTitle('Input error');
                $jk->setError('Isi Tidak Ada Dalam List');
                $jk->setPromptTitle('Pilih Dari List');
                $jk->setPrompt('Pilihlah Isi Dari List');
                $jk->setFormula1(sprintf('"%s"', implode(',', $jk_options)));

                // agama
                $agama = $event->sheet->getCell("{$agama_column}2")->getDataValidation();
                $agama->setType(DataValidation::TYPE_LIST);
                $agama->setErrorStyle(DataValidation::STYLE_INFORMATION);
                $agama->setAllowBlank(false);
                $agama->setShowInputMessage(true);
                $agama->setShowErrorMessage(true);
                $agama->setShowDropDown(true);
                $agama->setErrorTitle('Input error');
                $agama->setError('Isi Tidak Ada Dalam List');
                $agama->setPromptTitle('Pilih Dari List');
                $agama->setPrompt('Pilihlah Isi Dari List');
                $agama->setFormula1('=$R$2:$R$' . $no_akhir_agama);

                // pendidikan
                $pendidikan = $event->sheet->getCell("{$pendidikan_column}2")->getDataValidation();
                $pendidikan->setType(DataValidation::TYPE_LIST);
                $pendidikan->setErrorStyle(DataValidation::STYLE_INFORMATION);
                $pendidikan->setAllowBlank(false);
                $pendidikan->setShowInputMessage(true);
                $pendidikan->setShowErrorMessage(true);
                $pendidikan->setShowDropDown(true);
                $pendidikan->setErrorTitle('Input error');
                $pendidikan->setError('Isi Tidak Ada Dalam List');
                $pendidikan->setPromptTitle('Pilih Dari List');
                $pendidikan->setPrompt('Pilihlah Isi Dari List');
                $pendidikan->setFormula1('=$S$2:$S$' . $no_akhir_pendidikan);

                // pekerjaan
                $pekerjaan = $event->sheet->getCell("{$pekerjaan_column}2")->getDataValidation();
                $pekerjaan->setType(DataValidation::TYPE_LIST);
                $pekerjaan->setErrorStyle(DataValidation::STYLE_INFORMATION);
                $pekerjaan->setAllowBlank(false);
                $pekerjaan->setShowInputMessage(true);
                $pekerjaan->setShowErrorMessage(true);
                $pekerjaan->setShowDropDown(true);
                $pekerjaan->setErrorTitle('Input error');
                $pekerjaan->setError('Isi Tidak Ada Dalam List');
                $pekerjaan->setPromptTitle('Pilih Dari List');
                $pekerjaan->setPrompt('Pilihlah Isi Dari List');
                $pekerjaan->setFormula1('=$T$2:$T$' . $no_akhir_pekerjaan);

                // status perkawinan
                $status_perkawinan = $event->sheet->getCell("{$status_perkawinan_column}2")->getDataValidation();
                $status_perkawinan->setType(DataValidation::TYPE_LIST);
                $status_perkawinan->setErrorStyle(DataValidation::STYLE_INFORMATION);
                $status_perkawinan->setAllowBlank(false);
                $status_perkawinan->setShowInputMessage(true);
                $status_perkawinan->setShowErrorMessage(true);
                $status_perkawinan->setShowDropDown(true);
                $status_perkawinan->setErrorTitle('Input error');
                $status_perkawinan->setError('Isi Tidak Ada Dalam List');
                $status_perkawinan->setPromptTitle('Pilih Dari List');
                $status_perkawinan->setPrompt('Pilihlah Isi Dari List');
                $status_perkawinan->setFormula1(sprintf('"%s"', implode(',', $status_perkawinan_options)));

                // clone validation to remaining rows
                for ($i = 3; $i <= $row_count; $i++) {
                    // jenis kelamin
                    $event->sheet->getCell("{$jk_column}{$i}")->setDataValidation(clone $jk);

                    // agama
                    $event->sheet->getCell("{$agama_column}{$i}")->setDataValidation(clone $agama);

                    // pendidikan
                    $event->sheet->getCell("{$pendidikan_column}{$i}")->setDataValidation(clone $pendidikan);

                    // pekerjaan
                    $event->sheet->getCell("{$pekerjaan_column}{$i}")->setDataValidation(clone $pekerjaan);

                    // status perkawinan
                    $event->sheet->getCell("{$status_perkawinan_column}{$i}")->setDataValidation(clone $status_perkawinan);
                }
            },
        ];
    }

    public function headings(): array
    {
        return [
            'Nomor Kartu Keluarga',
            'Alamat',
            'Kode POS',
            'RT',
            'RW',
            'NIK Kepala Keluarga',
            'Nama Lengkap Kepala Keluarga',
            'Tempat Lahir Kepala Keluarga',
            'Tanggal Lahir Kepala Keluarga',
            'Jenis Kelamin Kepala Keluarga',
            'Agama Kepala Keluarga',
            'Pendidikan Kepala Keluarga',
            'Pekerjaan Kepala Keluarga',
            'Status Perkawinan Kepala Keluarga',
            'Nama Ayah Kepala Keluarga',
            'Nama Ibu Kepala Keluarga',
            '',
            'Data Agama Yang Terdaftar',
            'Data Pendidikan Yang Terdaftar',
            'Data Pekerjaan Yang Terdaftar'
        ];
    }

    public function columnWidths(): array
    {
        return [
            'Q' => 12,
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('A2')->getFont()->setBold(true);
        $sheet->getStyle('B1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('C1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('D1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('E1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('F1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('G1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('H1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('I1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('J1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('K1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('L1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('M1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('N1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('O1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('P1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('R1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('S1')->getFont()->setSize(14)->setBold(true);
        $sheet->getStyle('T1')->getFont()->setSize(14)->setBold(true);
    }

    public function title(): string
    {
        return 'Kartu Keluarga';
    }

    public function columnFormats(): array
    {
        return [
            'J' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }
}
