<?php

namespace App;

use DB;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class ObatAlkes extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        
    ];

    protected $table = 'obatalkes_m';

    public $timestamps = false;

    public static function findOne($where = array())
    {
        $find = DB::table('obatalkes_m')
            ->select('obatalkes_m.*')
            ->where($where)
            ->first();
        return $find;
    }

    public static function getAll($where = array())
    {
        $find = DB::table('obatalkes_m')
            ->select('obatalkes_m.*')
            ->where($where)
            ->get()->toArray();
        return $find;
    }

}
