<?php

namespace App;

use DB;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class RacikanDetail extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'racikan_id', 'obatalkes_id', 'qty'
    ];

    protected $table = 'racikan_detail';

    public static function findOne($where = array())
    {
        $find = DB::table('racikan_detail')
            ->select('racikan_detail.*', 'racikan.kode as racikan_kode', 'racikan.nama as racikan_nama', 'obatalkes_m.obatalkes_kode as obatalkes_kode', 'obatalkes_m.obatalkes_nama as obatalkes_nama', 'obatalkes_m.stok as obatalkes_stok')
            ->join('racikan','racikan.id','=','racikan_detail.racikan_id', 'left')
            ->join('obatalkes_m','obatalkes_m.obatalkes_id','=','racikan_detail.obatalkes_id', 'left')
            ->where($where)
            ->first();
        return $find;
    }

    public static function getAll($where = array())
    {
        $find = DB::table('racikan_detail')
            ->select('racikan_detail.*', 'racikan.kode as racikan_kode', 'racikan.nama as racikan_nama', 'obatalkes_m.obatalkes_kode as obatalkes_kode', 'obatalkes_m.obatalkes_nama as obatalkes_nama', 'obatalkes_m.stok as obatalkes_stok')
            ->join('racikan','racikan.id','=','racikan_detail.racikan_id', 'left')
            ->join('obatalkes_m','obatalkes_m.obatalkes_id','=','racikan_detail.obatalkes_id', 'left')
            ->where($where)
            ->get()->toArray();
        return $find;
    }

    public static function getByRacikanId($where = array()){
        $return = [];

        $find = DB::table('racikan_detail')
            ->select('racikan_detail.*', 'obatalkes_m.obatalkes_kode as obatalkes_kode', 'obatalkes_m.obatalkes_nama as obatalkes_nama', 'obatalkes_m.stok as obatalkes_stok')
            ->join('obatalkes_m','obatalkes_m.obatalkes_id','=','racikan_detail.obatalkes_id', 'left')
            ->where($where)
            ->get()->toArray();
        if($find){
            foreach ($find as $key => $value) {
                $return[$value->racikan_id][] = $value;
            }
        }

        return $return;
    }

}
