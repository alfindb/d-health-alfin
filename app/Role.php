<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "role";
    protected $fillable = ['role_name'];

    public static function findOne($where = array())
    {
        $find = DB::table('role')
            ->where($where)
            ->first();
        return $find;
    }

    public static function getAll($where = array())
    {
        $find = DB::table('role')
            ->where($where)
            ->get()->toArray();
        return $find;
    }

    public function user()
    {
       return $this->hasMany(User::class,'role_id');
    }
}
