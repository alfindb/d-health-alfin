<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;

class PendudukImport implements WithMultipleSheets, SkipsUnknownSheets
{
   
    public function sheets(): array
    {
        return [
            'Kartu Keluarga' => new KKImport(),
            'Anggota Kartu Keluarga' => new AnggotaKKImport()
        ];
    }

    public function onUnknownSheet($sheetName)
    {
        // E.g. you can log that a sheet was not found.
        info("Sheet {$sheetName} was skipped");
    }
}
