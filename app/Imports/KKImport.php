<?php

namespace App\Imports;

use App\KK;
use App\AnggotaKeluarga;
use App\Provinsi;
use App\KabupatenKota;
use App\KelurahanDesa;
use App\Agama;
use App\Pendidikan;
use App\Pekerjaan;
use App\SetProvinsiDesa;
use Hamcrest\Core\Set;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use \PhpOffice\PhpSpreadsheet\Shared\Date;

class KKImport implements ToCollection, WithMultipleSheets, WithCalculatedFormulas
{
    /**
     * @param Collection $collection
     */
    private $errors = array();
    private $errors_format = array();
    private $success = array();

    public function sheets(): array
    {
        return [
            'Kartu Keluarga' => $this,
        ];
    }

    public function collection(Collection $collection)
    {
        $check = SetProvinsiDesa::first();
        if (count((array)$check) > 0) {
            foreach ($collection as $index => $row) {
                if ($index > 2) {

                    if (empty($row[0]) || empty($row[1]) || empty($row[2]) || empty($row[3]) || empty($row[4]) || empty($row[5]) || empty($row[6]) || empty($row[7]) || empty($row[8]) || empty($row[9]) || empty($row[10]) || empty($row[11]) || empty($row[12]) || empty($row[13]) || empty($row[14]) || empty($row[15])) {
                    } else {
                        // cek input no_kk, rt, rw, nik value(numeric) & min 16 karakter(nik, no_kk)
                        if (is_numeric($row[0]) === true && is_numeric($row[3]) === true && is_numeric($row[4]) === true && is_numeric($row[5]) === true && strlen($row[0]) === 16 && strlen($row[5]) === 16) {
                            // check nomor kk
                            $check_kk = KK::where('no_kk', $row[0])->first();

                            // check nomor nik
                            $check_nik = AnggotaKeluarga::where('nik', $row[5])->first();

                            if (count((array)$check_kk) === 0 && count((array)$check_nik) === 0) {
                                // check agama
                                $check_agama = Agama::where('nama', $row[10])->first();

                                // check pendidikan
                                $check_pendidikan = Pendidikan::where('nama', $row[11])->first();

                                // check pekerjaan
                                $check_pekerjaan = Pekerjaan::where('nama', $row[12])->first();

                                if (count((array)$check_agama) > 0 && count((array)$check_pendidikan) > 0 && count((array)$check_pekerjaan) > 0) {
                                    $this->success = $row;
                                    $no_kk = $row[0];
                                    $m_kelurahan_desa = $check->m_kelurahan_desa;
                                    $alamat = $row[1];
                                    $kode_pos = $row[2];
                                    $rt = $row[3];
                                    $rw = $row[4];
                                    $nik = $row[5];
                                    $nama_lengkap = $row[6];
                                    $tempat_lahir = $row[7];
                                    $tanggal_lahir = $row[8];

                                    if ($row[9] === "Laki-laki") {
                                        $jk = 'L';
                                    } else {
                                        $jk = 'P';
                                    }

                                    if ($row[13] === "Kawin") {
                                        $sk = 'kawin';
                                    }
                                    else if ($row[13] === "Belum Kawin") {
                                        $sk = 'belum';
                                    }
                                    if ($row[13] === "Cerai Hidup") {
                                        $sk = 'cerai_hidup';
                                    }
                                    else {
                                        $sk = 'cerai_mati';
                                    }

                                    $m_agama = $check_agama->id;
                                    $m_pendidikan = $check_pendidikan->id;
                                    $m_pekerjaan = $check_pekerjaan->id;

                                    $kk = new KK;
                                    $kk->no_kk = $no_kk;
                                    $kk->alamat = $alamat;
                                    $kk->m_kelurahan_desa = $m_kelurahan_desa;
                                    $kk->rt = $rt;
                                    $kk->rw = $rw;
                                    $kk->kode_pos = $kode_pos;
                                    $kk->save();

                                    $anggota = new AnggotaKeluarga;
                                    $anggota->m_kartu_keluarga = $kk->id;
                                    $anggota->nik = $nik;
                                    $anggota->nama_lengkap = $nama_lengkap;
                                    $anggota->tempat_lahir = $tempat_lahir;
                                    $anggota->tanggal_lahir = Date::excelToDateTimeObject($tanggal_lahir);
                                    $anggota->jenis_kelamin = $jk;
                                    $anggota->sdhk = 'Kepala Keluarga';
                                    $anggota->status_perkawinan = $sk;
                                    $anggota->nama_ayah = $row[14];
                                    $anggota->nama_ibu = $row[15];
                                    $anggota->m_agama = $m_agama;
                                    $anggota->m_pendidikan = $m_pendidikan;
                                    $anggota->m_pekerjaan = $m_pekerjaan;
                                    $anggota->save();
                                } else {
                                    $this->errors_format[] = $row;
                                }
                            } else {
                                $this->errors[] = $row;
                            }
                        } else {
                            $this->errors_format[] = $row;
                        }
                    }
                }
            }
        }
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function getErrorsFormat(): array
    {
        return $this->errors_format;
    }

    public function getSuccess(): array
    {
        return $this->success;
    }
}
