<?php

namespace App\Imports;

use App\KK;
use App\AnggotaKeluarga;
use App\Provinsi;
use App\KabupatenKota;
use App\KelurahanDesa;
use App\Agama;
use App\Pendidikan;
use App\Pekerjaan;
use App\SetProvinsiDesa;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use \PhpOffice\PhpSpreadsheet\Shared\Date;

class AnggotaKKImport implements ToCollection, WithMultipleSheets, WithCalculatedFormulas
{
    /**
     * @param Collection $collection
     */
    private $errors = array();

    public function sheets(): array
    {
        return [
            'Anggota Kartu Keluarga' => $this,
        ];
    }

    public function collection(Collection $collection)
    {
        $check = SetProvinsiDesa::first();
        if (count((array)$check) > 0) {
            foreach ($collection as $index => $row) {
                if ($index > 3) {
                    if (empty($row[0]) || empty($row[1]) || empty($row[2]) || empty($row[3]) || empty($row[4]) || empty($row[5]) || empty($row[6]) || empty($row[7]) || empty($row[8]) || empty($row[9]) || empty($row[10]) || empty($row[11]) || empty($row[12])) {
                    } else {

                        // cek input no_kk, nik value(numeric) & min 16 karakter(nik, no_kk)
                        if (is_numeric($row[0]) === true && is_numeric($row[1]) === true && strlen($row[0]) === 16 && strlen($row[1]) === 16) {
                            // check nomor kk
                            $check_kk = KK::where('no_kk', $row[0])->first();

                            // check nomor nik
                            $check_nik = AnggotaKeluarga::where('nik', $row[1])->first();

                            if (count((array)$check_kk) > 0 && count((array)$check_nik) === 0) {

                                // check agama
                                $check_agama = Agama::where('nama', $row[7])->first();

                                // check pendidikan
                                $check_pendidikan = Pendidikan::where('nama', $row[8])->first();

                                // check pekerjaan
                                $check_pekerjaan = Pekerjaan::where('nama', $row[9])->first();

                                if (count((array)$check_agama) > 0 && count((array)$check_pendidikan) > 0 && count((array)$check_pekerjaan) > 0) {
                                    $nik = $row[1];
                                    $nama_lengkap = $row[2];
                                    $tempat_lahir = $row[3];
                                    $tanggal_lahir = $row[4];

                                    if ($row[5] === "Laki-laki") {
                                        $jk = 'L';
                                    } else {
                                        $jk = 'P';
                                    }

                                    if ($row[10] === "Kawin") {
                                        $sk = 'kawin';
                                    }
                                    else if ($row[10] === "Belum Kawin") {
                                        $sk = 'belum';
                                    }
                                    if ($row[10] === "Cerai Hidup") {
                                        $sk = 'cerai_hidup';
                                    }
                                    else {
                                        $sk = 'cerai_mati';
                                    }

                                    $sdhk = $row[6];
                                    $m_agama = $check_agama->id;
                                    $m_pendidikan = $check_pendidikan->id;
                                    $m_pekerjaan = $check_pekerjaan->id;

                                    $anggota = new AnggotaKeluarga;
                                    $anggota->m_kartu_keluarga = $check_kk->id;
                                    $anggota->nik = $nik;
                                    $anggota->nama_lengkap = $nama_lengkap;
                                    $anggota->tempat_lahir = $tempat_lahir;
                                    $anggota->tanggal_lahir = Date::excelToDateTimeObject($tanggal_lahir);
                                    $anggota->jenis_kelamin = $jk;
                                    $anggota->sdhk = $sdhk;
                                    $anggota->status_perkawinan = $sk;
                                    $anggota->nama_ayah = $row[11];
                                    $anggota->nama_ibu = $row[12];
                                    $anggota->m_agama = $m_agama;
                                    $anggota->m_pendidikan = $m_pendidikan;
                                    $anggota->m_pekerjaan = $m_pekerjaan;
                                    $anggota->save();
                                }
                            } else {
                                $this->errors[] = $row->toArray();
                            }
                        }
                    }
                }
            }
        }
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
