<?php

namespace App;

use DB;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Resep extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'nama_pasien', 'tanggal', 'detail', 'total_stok'
    ];

    protected $table = 'resep';

    public static function findOne($where = array())
    {
        $find = DB::table('resep')
            ->select('resep.*')
            ->where($where)
            ->first();
        return $find;
    }

    public static function getAll($where = array())
    {
        $find = DB::table('resep')
            ->select('resep.*')
            ->where($where)
            ->get()->toArray();
        return $find;
    }

}
