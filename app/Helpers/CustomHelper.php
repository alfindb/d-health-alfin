<?php
// Function Date Format 21 Oktober 2019
function date_indo($date)
{
    $month = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$data = explode('-', $date);
 
	return $data[2] . ' ' . $month[ (int)$data[1] ] . ' ' . $data[0];
}

function short_date_indo($date)
{
	$month = array (
		1 =>   'Jan',
		'Feb',
		'Mar',
		'Apr',
		'Mei',
		'Jun',
		'Jul',
		'Agu',
		'Sep',
		'Okt',
		'Nov',
		'Des'
	);
	$data = explode('-', $date);
 
	return $data[2] . '-' . $month[ (int)$data[1] ] . '-' . $data[0];
}