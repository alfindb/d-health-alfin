<?php
namespace App\Helpers;
use App\Log;
use Illuminate\Support\Facades\DB;

class Logs{
    public static function add($action, $description)
    {
        $log = new Log();
        $log->id = $log->id;
        $log->user_id = auth()->user()->id;
        $log->action = $action;
        $log->description = $description;
        $log->save();
    }
}