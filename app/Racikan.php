<?php

namespace App;

use DB;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Racikan extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'kode', 'nama', 'is_active'
    ];

    protected $table = 'racikan';

    public static function findOne($where = array())
    {
        $find = DB::table('racikan')
            ->select('racikan.*')
            ->where($where)
            ->first();
        return $find;
    }

    public static function getAll($where = array())
    {
        $find = DB::table('racikan')
            ->select('racikan.*')
            ->where($where)
            ->get()->toArray();
        return $find;
    }

}
