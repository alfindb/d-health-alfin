<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use DB;
use PDF;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        return view('user.index');
    }

    public function json()
    {
        $user = User::getAll();
        return Datatables::of($user)
            ->addColumn('join_date', function ($row) {
                $format_date = date('d-M-Y', strtotime($row->join_date));
                return $format_date;
            })
            ->addColumn('action', function ($row) {

                $lihat_button = "<a href='" . url('user/detail/') . "/" . $row->id . "' class='btn btn-primary btn-sm'>Lihat</a>";
                $edit_button = "<a href='" . url('user/edit/') . "/" . $row->id . "' class='btn btn-warning btn-sm' >Ubah</a>";
                $print_button = "<a href='" . url('/user/print/') . "/" . $row->id . "' class='btn btn-success btn-sm' >Print</a>";
                // $hapus_button = "<a class='btn btn-danger btn-sm delete' data-url='" . url('pengaturan_penerbitan_surat_delete') . "/" . $row->id . "'>Hapus</a>";
                $hapus_button = '';

                $btn = $lihat_button . " " . $edit_button . " " . $print_button . " " . $hapus_button;
                return $btn;
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function add(){
        $data = array();
        $role = Role::getAll();

        $data['role'] = $role;

        return view('user.add', $data);
    }

    public function save(Request $request){
        if ($request->post()) {

            $save_data = [
                'full_name' => $request->post('full_name'),
                'email' => $request->post('email'),
                'password' => bcrypt($request->post('password')),
                'username' => $request->post('username'),
                'role_id' => $request->post('role'),
                'join_date' => $request->post('join_date')
            ];
            $insert_data = User::create($save_data);
            $insert_data->save();

            if($insert_data){
                return redirect()->route('user')->with(['success' => 'Berhasil menyimpan data']);;
            }else{
                return redirect()->route('user')->with(['failed' => 'Gagal menyimpan data']);;
            }

        } else {
            return redirect()->route('user')->with(['failed' => 'Terdapat kesalahan dalam pengisian form']);;
        }
    }

    public function detail($id){
        $user = User::findOne(['users.id' => $id]);
        $data['user'] = $user;

        return view('user.detail', $data);
    }

    public function edit($id){
        $user = User::findOne(['users.id' => $id]);
        $role = Role::getAll();

        $data['user'] = $user;
        $data['role'] = $role;
        return view('user.edit', $data);
    }

    public function update(Request $request){
        if ($request->post()) {

            $data_update = [
                'full_name' => $request->post('full_name'),
                'email' => $request->post('email'),
                'username' => $request->post('username'),
                'role_id' => $request->post('role'),
                'join_date' => $request->post('join_date')
            ];
            $update_data = User::where('id', $request->post('id'))->update($data_update);

            if($update_data){
                return redirect()->route('user')->with(['success' => 'Berhasil melakukan update data']);;
            }else{
                return redirect()->route('user')->with(['failed' => 'Gagal melakukan update data']);;
            }

        } else {
            return redirect()->route('user')->with(['failed' => 'Terdapat kesalahan dalam pengisian form']);;
        }
    }

    public function checkIsExist(Request $request){
        $where = [];
        $tipe = $request->segment(3);
        $id = $request->segment(4);

        //cek apakah tipenya edit
        if($id){
            $data_user = User::findOne(['users.id' => $id]);
            $last_username = $data_user->username;
            $last_email = $data_user->email;

            if($tipe == 1){
                $username = $request->get('username');
                if($last_username == $username){
                    return 'true';
                }else{
                    $where['users.username'] = $username;
                    $result = self::checkUsernameOrEmail($where);
                }
            }else{
                $email = $request->get('email');
                if($last_email == $email){
                    return 'true';
                }else{
                    $where['users.email'] = $email;
                    $result = self::checkUsernameOrEmail($where);
                }
            }

        }else{
            if($tipe == 1){
                $username = $request->get('username');
                $where['users.username'] = $username;
                $result = self::checkUsernameOrEmail($where);
            }else{
                $email = $request->get('email');
                $where['users.email'] = $email;
                $result = self::checkUsernameOrEmail($where);
            }
        }

        return $result;
    }

    public function checkUsernameOrEmail($where){
        $check = User::findOne($where);
        if($check){
            return 'false';
        }else{
            return 'true';
        }
    }

    public function print(Request $request){
        $type = $request->id;
        if($type == 0){
            $users = User::getAll();
            $nama_dokumen = 'Daftar Semua User';
        }else{
            $users = User::findOne(['users.id' => $request->id]);
            $nama_dokumen = 'Cetak User '.$users->full_name;
        }
        $view = 'user/print';

        $pdf = PDF::loadview($view, ['users' => $users, 'type' => $type]);
        $pdf->setPaper('A4');

        return $pdf->download($nama_dokumen . '.pdf');
        // return $pdf->stream("filename.pdf", array("Attachment" => false));
    }
}
