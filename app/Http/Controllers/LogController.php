<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Log;
use DataTables;

class LogController extends Controller
{
    public function json()
    {
        $log = Log::orderBy('id', 'DESC')->get();
        return Datatables::of($log)
            ->addColumn('user', function ($row){
                return $row->user->full_name;
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function index()
    {
        return view('log.index');
    }
}
