<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use DB;
use PDF;
use App\ObatAlkes;
use App\Signa;
use App\Racikan;
use App\RacikanDetail;
use App\Resep;
use App\StokTemporary;
use Illuminate\Http\Request;

class ResepController extends Controller
{
    public function index(Request $request)
    {
        return view('resep.index');
    }

    public function dtJson()
    {
        $data = Resep::getAll();
        return Datatables::of($data)
            ->addColumn('tanggal', function ($row) {
                return date('d-m-Y', strtotime($row->tanggal));
            })
            ->addColumn('total_stok', function ($row) {
                return number_format($row->total_stok);
            })
            ->addColumn('action', function ($row) {

                $lihat_button = "<a href='" . url('resep/detail') . "/" . $row->id . "' class='btn btn-primary btn-sm'><i class='fa fa-search white-text'></i></a>";
                $print_button = "<a href='" . url('resep/print') . "/" . $row->id . "' class='btn btn-info btn-sm'><i class='fa fa-print white-text'></i></a>";

                $btn = $lihat_button." ".$print_button;
                return $btn;
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function add(){
        $data = array();

        /* delete temporary */
        $delete = StokTemporary::where('created_by', Auth::user()->id)->delete();

        return view('resep.add', $data);
    }

    public function save(Request $request){
        if ($request->post()) {
            $save_data = [
                'nama_pasien' => $request->nama_pasien,
                'tanggal' => date('Y-m-d'),
                'detail' => json_encode($request->detail),
                'total_stok' => $request->total_stok
            ];
            $insert_data = Resep::create($save_data);
            $insert_data->save();
            $id = $insert_data->id;

            if($insert_data){
                $detail = $request->detail;
                if($detail){
                    foreach ($detail as $key => $value) {
                        $val = json_decode($value);
                        if($val[0]->tipe == 1){
                            self::reduceStock($val[0]->obat_id, $val[0]->qty);
                        }elseif($val[0]->tipe == 2){
                            if($val[0]->detail){
                                foreach ($val[0]->detail as $dt => $val_dt) {
                                    self::reduceStock($val_dt->obatalkes_id, $val_dt->qty);
                                }
                            }
                        }
                    }
                }

                /* delete temp table */
                StokTemporary::where('created_by', Auth::user()->id)->delete();

                return redirect()->route('resep')->with(['success' => 'Berhasil menyimpan data']);;
            }else{
                return redirect()->route('resep')->with(['failed' => 'Gagal menyimpan data']);;
            }

        } else {
            return redirect()->route('resep')->with(['failed' => 'Terdapat kesalahan dalam pengisian form']);;
        }
    }

    public function detail($id){
        $data['resep'] = Resep::findOne(['resep.id' => $id]);
        $data['resep_detail'] = [];
        $resep_detail = json_decode($data['resep']->detail);
        if($resep_detail){
            foreach ($resep_detail as $key => $value) {
                $val = json_decode($value);
                array_push($data['resep_detail'], $val[0]);
            }
        }

        return view('resep.detail', $data);
    }

    public function print(Request $request){
        $id = $request->id;
        $data['resep'] = Resep::findOne(['resep.id' => $id]);
        $data['resep_detail'] = [];
        $resep_detail = json_decode($data['resep']->detail);
        if($resep_detail){
            foreach ($resep_detail as $key => $value) {
                $val = json_decode($value);
                array_push($data['resep_detail'], $val[0]);
            }
        }
        $nama_dokumen = "daftar_resep_".$data['resep']->nama_pasien;
        $view = 'resep/print';

        $pdf = PDF::loadview($view, $data);
        $pdf->setPaper('A4');

        return $pdf->download($nama_dokumen . '.pdf');
        // return $pdf->stream("filename.pdf", array("Attachment" => false));
    }

    public function getByType(Request $request){
        $status = false;
        $data = [];
        $message = 'Gagal Mengambil Data';

        $tipe = $request->tipe;
        
        if($tipe == 1){
            $obat = ObatAlkes::getAll();
            $data['obat'] = $obat;
            $data['signa'] = Signa::getAll();
            $status = true;
            $message = 'Berhasil Mengambil Data';
        }elseif($tipe == 2){
            $racikan = Racikan::getAll();
            // $racikan_detail = RacikanDetail::getByRacikanId();
            
            // if($racikan){
            //     foreach ($racikan as $key => $value) {
            //         $racikan[$key]->detail = [];
            //         if(isset($racikan_detail[$value->id])){
            //             $racikan[$key]->detail = $racikan_detail[$value->id];
            //         }
            //     }
            // }
            $data['racikan'] = $racikan;
            $data['signa'] = Signa::getAll();

            $status = true;
            $message = 'Berhasil Mengambil Data';
        }

        return response()->json(['status' => $status, 'data' => $data, 'message'=> $message]);
    }

    public function getStokObat(Request $request){
        $status = false;
        $data = [];
        $message = 'Gagal Mengambil Data';

        $user_id = Auth::user()->id;
        $id = $request->id;

        $stok_temp = 0;
        $get_stok_temp = StokTemporary::getOneSum(['stok_temporary.obatalkes_id' => $id]);
        if($get_stok_temp){
            $stok_temp = $get_stok_temp->stok;
        }

        $obat = ObatAlkes::findOne(['obatalkes_m.obatalkes_id' => $id]);
        if($obat){
            $final_stok = $obat->stok - $stok_temp;
            $status = true;
            $data = $final_stok;
            $message = 'Berhasil mengambil data';
        }
        
        return response()->json(['status' => $status, 'data' => $data, 'message'=> $message]);
    }
    
    public function getStokRacikan(Request $request){
        $status = false;
        $data = [];
        $message = 'Gagal Mengambil Data';
        
        $user_id = Auth::user()->id;
        $id = $request->id;
        
        $get_racikan_detail = RacikanDetail::getAll(['racikan_detail.racikan_id' => $id]);
        if($get_racikan_detail){
            foreach ($get_racikan_detail as $key => $value) {
                $get_racikan_detail[$key]->stok_live = $value->obatalkes_stok;
                $get_stok_temp = StokTemporary::getOneSum(['stok_temporary.obatalkes_id' => $value->obatalkes_id]);
                if($get_stok_temp){
                    $get_racikan_detail[$key]->stok_live = $value->obatalkes_stok - $get_stok_temp->stok;
                }
            }
            $status = true;
            $data = $get_racikan_detail;
            $message = 'Berhasil mengambil data';

        }

        return response()->json(['status' => $status, 'data' => $data, 'message'=> $message]);
    }

    public function addObat(Request $request){
        $status = false;
        $data = [];
        $message = 'Gagal Menyimpan Data';
        $user_id = Auth::user()->id;
        $obat_id = $request->obat_id;
        $qty = $request->qty;

        // add temporary
        $data = [
            'obatalkes_id' => $obat_id,
            'stok' => $qty,
            'created_by' => $user_id
        ];
        $insert_data = StokTemporary::create($data);
        $insert_data->save();
        $id = $insert_data->id;

        if($insert_data){
            $status = true;
            $data = $id;
            $message = 'Berhasil Menyimpan Data';
        }

        return response()->json(['status' => $status, 'data' => $data, 'message'=> $message]);
    }

    public function deleteObat(Request $request){
        $status = false;
        $data = [];
        $message = 'Gagal Menghapus Data';
        $id = $request->id;

        // delete temporary
        $get_data = StokTemporary::where('id', $id)->first();
        $delete = StokTemporary::where('id', $id)->delete();

        if($delete){
            $status = true;
            $data = $get_data->stok;
            $message = 'Berhasil Menghapus Data';
        }

        return response()->json(['status' => $status, 'data' => $data, 'message'=> $message]);
    }

    public function addRacikan(Request $request){
        $status = false;
        $data = [];
        $message = 'Gagal Menyimpan Data';
        $user_id = Auth::user()->id;

        $array_racikan = $request->array_racikan_input;
        if($array_racikan){
            foreach ($array_racikan as $key => $value) {
                // add temporary
                $data_insert = [
                    'obatalkes_id' => $value['obatalkes_id'],
                    'stok' => $value['qty'],
                    'created_by' => $user_id
                ];
                $insert_data = StokTemporary::create($data_insert);
                $insert_data->save();
                $id = $insert_data->id;

                array_push($data, $id);
            }
        }

        if($data){
            $status = true;
            $message = 'Berhasil Menyimpan Data';
        }

        return response()->json(['status' => $status, 'data' => $data, 'message'=> $message]);
    }

    public function deleteRacikan(Request $request){
        $status = false;
        $data = [];
        $message = 'Gagal Menghapus Data';
        $id = $request->id;
        $stok = 0;

        if($id){
            foreach ($id as $key => $value) {
                // delete temporary
                $get_data = StokTemporary::where('id', $value)->first();
                $delete = StokTemporary::where('id', $value)->delete();

                $stok += $get_data->stok;
            }
        }

        if($stok > 0){
            $status = true;
            $data = $stok;
            $message = 'Berhasil Menghapus Data';
        }

        return response()->json(['status' => $status, 'data' => $data, 'message'=> $message]);
    }

    private static function reduceStock($id, $qty){
        $get_data = ObatAlkes::findOne(['obatalkes_id' => $id]);
        $final_stok = $get_data->stok - $qty;

        /* update */
        $data_update = [
            'stok' => $final_stok
        ];
        $update = ObatAlkes::where('obatalkes_id', $id)->update($data_update);
    }
}
