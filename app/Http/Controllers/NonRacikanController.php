<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use DB;
use App\ObatAlkes;
use Illuminate\Http\Request;

class NonRacikanController extends Controller
{
    public function index(Request $request)
    {
        return view('non_racikan.index');
    }

    public function dtJson()
    {
        $data = ObatAlkes::getAll();
        return Datatables::of($data)
            ->addColumn('stok', function ($row) {
                return number_format($row->stok);
            })
            ->addIndexColumn()
            ->make(true);
    }
}
