<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use LogAdd;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            if (Auth::user()->username === $request->username) {
                LogAdd::add('Login', 'Login Berhasil');
                return redirect('/dashboard');
            } else {
                Session::flush();
                return redirect('/')->with('failed', 'Username atau Password Anda Salah');
            }
        } else {
            return redirect('/')->with('failed', 'Username atau Password Anda Salah');
        }
    }

    public function logout()
    {
        LogAdd::add('Logout', 'Logout Berhasil');
        Auth::logout();
        session()->flush();
        return redirect('/');
    }
}
