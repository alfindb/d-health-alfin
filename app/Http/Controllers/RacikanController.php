<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use DB;
use App\ObatAlkes;
use App\Racikan;
use App\RacikanDetail;
use Illuminate\Http\Request;

class RacikanController extends Controller
{
    public function index(Request $request)
    {
        return view('racikan.index');
    }

    public function dtJson()
    {
        $data = Racikan::getAll();
        return Datatables::of($data)
            ->addColumn('action', function ($row) {

                $lihat_button = "<a href='" . url('racikan/detail/') . "/" . $row->id . "' class='btn btn-primary btn-sm'><i class='fa fa-search white-text'></i></a>";
                $edit_button = "<a href='" . url('racikan/edit/') . "/" . $row->id . "' class='btn btn-warning btn-sm' ><i class='fa fa-edit white-text'></i></a>";

                $btn = $lihat_button . " " . $edit_button;
                return $btn;
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function add(){
        $data = array();
        $data['obat'] = ObatAlkes::getAll();

        return view('racikan.add', $data);
    }

    public function save(Request $request){
        if ($request->post()) {

            $save_data = [
                'kode' => $request->post('kode'),
                'nama' => $request->post('nama'),
            ];
            $insert_data = Racikan::create($save_data);
            $insert_data->save();
            $id = $insert_data->id;

            if($insert_data){
                $obatalkes_id = $request->obatalkes_id;
                $qty = $request->qty;

                if($obatalkes_id){
                    foreach ($obatalkes_id as $key => $value) {
                        $data_detail = [
                            'racikan_id' => $id,
                            'obatalkes_id' => $value,
                            'qty' => $qty[$key]
                        ];
                        $insert_detail = RacikanDetail::create($data_detail);
                        $insert_detail->save();
                    }
                }

                return redirect()->route('racikan')->with(['success' => 'Berhasil menyimpan data']);;
            }else{
                return redirect()->route('racikan')->with(['failed' => 'Gagal menyimpan data']);;
            }

        } else {
            return redirect()->route('racikan')->with(['failed' => 'Terdapat kesalahan dalam pengisian form']);;
        }
    }

    public function detail($id){
        $data['racikan'] = Racikan::findOne(['racikan.id' => $id]);
        $data['racikan_detail'] = RacikanDetail::getAll(['racikan_detail.racikan_id' => $id]);

        return view('racikan.detail', $data);
    }

    public function edit($id){
        $data['obat'] = ObatAlkes::getAll();
        $data['racikan'] = Racikan::findOne(['racikan.id' => $id]);
        $data['racikan_detail'] = RacikanDetail::getAll(['racikan_detail.racikan_id' => $id]);

        return view('racikan.edit', $data);
    }

    public function update(Request $request){
        if ($request->post()) {
            $data_update = [
                'kode' => $request->post('kode'),
                'nama' => $request->post('nama'),
            ];
            $update = Racikan::where('id', $request->id)->update($data_update);
            if($update){
                /* reset */
                $delete_detail = RacikanDetail::where('racikan_id', $request->id)->delete();

                $obatalkes_id = $request->obatalkes_id;
                $qty = $request->qty;

                if($obatalkes_id){
                    foreach ($obatalkes_id as $key => $value) {
                        $data_detail = [
                            'racikan_id' => $request->id,
                            'obatalkes_id' => $value,
                            'qty' => $qty[$key]
                        ];
                        $insert_detail = RacikanDetail::create($data_detail);
                        $insert_detail->save();
                    }
                }
                
                return redirect()->route('racikan')->with(['success' => 'Berhasil melakukan update data']);;
            }else{
                return redirect()->route('racikan')->with(['failed' => 'Gagal melakukan update data']);;
            }

        } else {
            return redirect()->route('racikan')->with(['failed' => 'Terdapat kesalahan dalam pengisian form']);;
        }
    }

    public function checkIsExist(Request $request){
        $where = [];
        $tipe = $request->segment(3);
        $id = $request->segment(4);

        //cek apakah tipenya edit
        if($id){
            $data_user = User::findOne(['users.id' => $id]);
            $last_username = $data_user->username;
            $last_email = $data_user->email;

            if($tipe == 1){
                $username = $request->get('username');
                if($last_username == $username){
                    return 'true';
                }else{
                    $where['users.username'] = $username;
                    $result = self::checkUsernameOrEmail($where);
                }
            }else{
                $email = $request->get('email');
                if($last_email == $email){
                    return 'true';
                }else{
                    $where['users.email'] = $email;
                    $result = self::checkUsernameOrEmail($where);
                }
            }

        }else{
            if($tipe == 1){
                $username = $request->get('username');
                $where['users.username'] = $username;
                $result = self::checkUsernameOrEmail($where);
            }else{
                $email = $request->get('email');
                $where['users.email'] = $email;
                $result = self::checkUsernameOrEmail($where);
            }
        }

        return $result;
    }

    public function checkUsernameOrEmail($where){
        $check = User::findOne($where);
        if($check){
            return 'false';
        }else{
            return 'true';
        }
    }
}
