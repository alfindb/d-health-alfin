<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use DB;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        return view('dashboard');
    }

    public function countUsersByYear(Request $request){
    	$year = $request->year;
    	$array_total = [0,0,0,0,0,0,0,0,0,0,0,0];
    	$users = User::countUsersByMonth($year);
    	if($users){
    		foreach ($users as $key => $value) {
    			$index_total = $value->month - 1;
    			$array_total[$index_total] = (int)$value->total;
    		}
    	}
    	$data['year_text'] = $year;
    	$data['array_total'] = $array_total;

    	return response()->json([
                'success' => true,
                'message' => 'Berhasil Mengambil Data',
                'data' => $data
            ]);
    }
}
