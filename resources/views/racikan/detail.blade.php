@extends('layout/master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Racikan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/racikan') }}">Data Racikan</a></li>
                        <li class="breadcrumb-item active">Detail Racikan</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                        <div class="card-header">
                        <h3 class="card-title">Detail Racikan</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ url('racikan/save') }}" method="POST" id="form">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="">Kode</label>
                                    <input type="text" name="kode" class="form-control" id="kode" placeholder="Kode Obat Racikan" value="{{ $racikan->kode }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="">Nama</label>
                                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Obat Racikan" value="{{ $racikan->nama }}"" disabled>
                                </div>
                                <hr>
                                <div id="div_obat">
                                    <h2>List Bahan</h2>
                                    @if ($racikan_detail)
                                        @foreach ($racikan_detail as $key => $val)
                                            <div id="div_{{ $key }}">
                                                <hr>
                                                <div class="row form-group" >
                                                    <div class="col-md-6">
                                                        <label for="">Obat</label>
                                                        <input type="text" class="form-control" value="{{ $val->obatalkes_nama }}" disabled>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="">Quantity</label>
                                                        <input type="number" class="form-control" name="qty[]" value="{{ $val->qty }}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class="float-right">
                                    <a href="{{ url('/racikan') }}" class="btn btn-danger">Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
                <!-- /.card -->
                </div>
                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-6">

                </div>
                <!--/.col (right) -->
            </div>
        <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

@endsection
@push('script')
<script>
    
</script>
@endpush
