@extends('layout/master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Racikan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/racikan') }}">Data Racikan</a></li>
                        <li class="breadcrumb-item active">Edit Racikan</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                        <div class="card-header">
                        <h3 class="card-title">Edit Racikan</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ url('racikan/update') }}" method="POST" id="form">
                            @csrf
                            <input type="hidden" name="id" value="{{ $racikan->id }}">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="">Kode</label>
                                    <input type="text" name="kode" class="form-control" id="kode" placeholder="Kode Obat Racikan" value="{{ $racikan->kode }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Nama</label>
                                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Obat Racikan" value="{{ $racikan->nama }}"" required>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <button type="button" class="btn btn-info" id="add_bahan">Add Bahan</button>
                                </div>
                                <div id="div_obat">
                                    @if ($racikan_detail)
                                        @foreach ($racikan_detail as $key => $val)
                                            <div id="div_{{ $key }}">
                                                <hr>
                                                <div class="row form-group" >
                                                    <div class="col-md-6">
                                                        <label for="">Obat</label>
                                                        <select name="obatalkes_id[]" class="form-control select2bs4" style="width: 100%;" required>
                                                            <option value="">-- Silakan Pilih Bahan --</option>
                                                            @if ($obat)
                                                                @foreach ($obat as $key_obat => $val_obat)
                                                                    <option value="{{ $val_obat->obatalkes_id }}" {{ $val_obat->obatalkes_id == $val->obatalkes_id ? 'selected' : '' }} >{{ $val_obat->obatalkes_nama }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <label for="">Quantity</label>
                                                        <input type="number" class="form-control" name="qty[]" value="{{ $val->qty }}" required>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label for="">&nbsp</label>
                                                        <button type="button" class="btn btn-danger btn-sm form-control btn-delete" data-row="{{ $key }}"><i class="fas fa-trash"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class="float-right">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <a href="{{ url('/racikan') }}" class="btn btn-danger">Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
                <!-- /.card -->
                </div>
                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-6">

                </div>
                <!--/.col (right) -->
            </div>
        <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<input type="hidden" id="list_obat" value="{{ json_encode($obat) }}">

@endsection
@push('script')
<script>
    let row = parseInt("{{ count($racikan_detail) }}") - 1;
    console.log(row)

    function initSelect2(){
        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
    }

    function addBahan(){
        $('#add_bahan').on('click', function () {
            const list_obat = JSON.parse( $('#list_obat').val() );
            let option = '';
            for (let i = 0; i < list_obat.length; i++) {                
                option += `<option value="`+list_obat[i].obatalkes_id+`">`+list_obat[i].obatalkes_nama+`</option>`;
            }
            
            let html = `
                <div id="div_`+row+`">
                    <hr>
                    <div class="row form-group" >
                        <div class="col-md-6">
                            <label for="">Obat</label>
                            <select name="obatalkes_id[]" class="form-control select2bs4" style="width: 100%;" required>
                                <option value="">-- Silakan Pilih Bahan --</option>
                                `+option+`
                            </select>
                        </div>
                        <div class="col-md-5">
                            <label for="">Quantity</label>
                            <input type="number" class="form-control" name="qty[]" value="1" required>
                        </div>
                        <div class="col-md-1">
                            <label for="">&nbsp</label>
                            <button type="button" class="btn btn-danger btn-sm form-control btn-delete" data-row="`+row+`"><i class="fas fa-trash"></i></button>
                        </div>
                    </div>
                </div>
            `;
            $('#div_obat').append(html);

            initSelect2();
            onClickDelete();
            row++;
        });
    }

    function onClickDelete(){
        $('.btn-delete').on('click', function () {
            const get_row = $(this).data('row');

            $('#div_'+get_row).remove();
        });
    }

    function formValidation(){
        $('#form').validate({
            rules: {
                kode: {
                    required: true
                },
                nama: {
                    required: true
                },
            },
            messages: {
                kode: {
                    required: "Kode tidak boleh kosong"
                },
                nama: {
                    required: "Nama tidak boleh kosong"
                },
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
              error.addClass('invalid-feedback');
              element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
              $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
              $(element).removeClass('is-invalid');
            }
        });
    }

    $(document).ready(function () {
        addBahan();
        onClickDelete();
        // formValidation();
    });
</script>
@endpush
