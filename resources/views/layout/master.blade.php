<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>DHealth</title>

    @include('layout.style')
</head>
<!--
`body` tag options:

  Apply one or more of the following classes to to the body tag
  to get the desired effect

  * sidebar-collapse
  * sidebar-mini
-->
<body class="hold-transition sidebar-mini">
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}">
                <i class="fas fa-sign-out-alt"></i>
                </a>
            </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
            <img src="{{ asset('backend/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">Basecode Laravel 7</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                <img src="{{ asset('backend/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->full_name }}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                @php
                    $dashboard_stat = '';
                    $non_racikan_stat = '';
                    $racikan_stat = '';
                    $resep_stat = '';

                    /* dashboard */
                    if( Request::is('dashboard') ){
                        $dashboard_stat = 'active';
                    }

                    /* non racikan */
                    if( Request::is('non_racikan') ){
                        $non_racikan_stat = 'active';
                    }

                    /* racikan */
                    if( Request::is('racikan') ||
                        Request::is('racikan/add') ||
                        Request::is('racikan/edit/*') ||
                        Request::is('racikan/detail/*')
                    ){
                        $racikan_stat = 'active';
                    }

                    /* resep */
                    if( Request::is('resep') ||
                        Request::is('resep/add') ||
                        Request::is('resep/detail/*')
                    ){
                        $resep_stat = 'active';
                    }

                @endphp
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item">
                        <a href="{{ route('dashboard') }}" class="nav-link {{ $dashboard_stat }}">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Dashboard
                            </p>
                        </a>
                    </li>
                    <li class="nav-header">MASTER DATA</li>
                    <li class="nav-item">
                        <a href="{{ url('/non_racikan') }}" class="nav-link {{ $non_racikan_stat }}">
                            <i class="nav-icon fas fa-tablets"></i>
                            <p>
                                Non Racikan
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/racikan') }}" class="nav-link {{ $racikan_stat }}">
                            <i class="nav-icon fas fa-pills"></i>
                            <p>
                                Racikan
                            </p>
                        </a>
                    </li>
                    <li class="nav-header">TRANSAKSI</li>
                    <li class="nav-item">
                        <a href="{{ url('/resep') }}" class="nav-link {{ $resep_stat }}">
                            <i class="nav-icon fas fa-prescription-bottle"></i>
                            <p>
                                Resep
                            </p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        @yield('content')
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->

        @include('layout.footer')

    </div>
<!-- ./wrapper -->

    @include('layout.script')
    @stack('script')
</body>
</html>
