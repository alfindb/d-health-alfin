<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>{{ Request::is('dashboard') ? 'Dasbor' : ''}}{{ Request::is('surat_masuk') || Request::is('surat_masuk_add') || Request::is('surat_masuk_edit/*') || Request::is('surat_masuk_detail/*') ? 'Surat Masuk' : '' }}{{ Request::is('surat_keluar') || Request::is('surat_keluar_add') || Request::is('surat_keluar_edit/*') || Request::is('surat_keluar_detail/*') ? 'Surat Keluar' : '' }}{{ Request::is('data_penduduk') || Request::is('data_penduduk_add') || Request::is('data_penduduk_edit/*') || Request::is('data_penduduk_detail/*') || Request::is('data_penduduk_import') || Request::is('data_penduduk/validasi_import') ? 'Data Penduduk' : '' }}{{ Request::is('pengaturan_pengguna') || Request::is('pengaturan_pengguna_add') || Request::is('pengaturan_pengguna_edit/*') ? 'Pengguna' : '' }}{{ Request::is('pengaturan_perihal') || Request::is('pengaturan_perihal_add') || Request::is('pengaturan_perihal_edit/*') ? 'Pengaturan Perihal' : '' }}{{ Request::is('pengaturan_penerbitan_surat') || Request::is('pengaturan_penerbitan_surat_add') || Request::is('pengaturan_penerbitan_surat_edit/*') || Request::is('pengaturan_penerbitan_surat_detail/*') ? 'Pengaturan Penerbitan Surat' : '' }}{{ Request::is('pengaturan_terima_surat') || Request::is('pengaturan_terima_surat_add') || Request::is('pengaturan_terima_surat_edit/*') ? 'Referensi Terima Surat' : '' }}{{ Request::is('pengaturan_provdes') ? 'Pengaturan Provinsi - Desa' : ''}}{{Request::is('pengaturan_log') ? 'Log' : ''}}@yield('title') - Smartdesa</title>
    <link rel="shortcut icon" href="{{asset('assets/images/favico.png')}}" />
    <!-- start: META -->
    <!-- <meta charset="utf-8" /> -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    @yield('head')
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Login - Smartdesa" name="description" />
    <meta content="Kabayan" name="author" />
    <!-- end: META -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />


    <link type="text/css" rel="stylesheet" href="{!! asset('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}">
    <link type="text/css" rel="stylesheet" href="{!! asset('bower_components/font-awesome/css/font-awesome.min.css') !!}" />
    <link type="text/css" rel="stylesheet" href="{!! asset('assets/fonts/clip-font.min.css') !!}" />


    <link type="text/css" rel="stylesheet" href="{!! asset('bower_components/sweetalert/dist/sweetalert.css') !!}" />
    <link type="text/css" rel="stylesheet" href="{!! asset('bower_components/iCheck/skins/all.css') !!}" />
    <link type="text/css" rel="stylesheet" href="{!! asset('bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css') !!}" />

    <link type="text/css" rel="stylesheet" href="{!! asset('assets/css/main.min.css') !!}" />
    <link type="text/css" rel="stylesheet" href="{!! asset('assets/css/main-responsive.min.css') !!}" />
    <!-- Custom -->
    <link type="text/css" rel="stylesheet" href="{!! asset('css/custom.css') !!}" />
    <link type="text/css" rel="stylesheet" media="print" href="{!! asset('assets/css/print.min.css') !!}" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="{!! asset('assets/css/theme/light.min.css') !!}" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="{!! asset('assets/css/checked.css') !!}" />
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="{!! asset('bower_components/select2/dist/css/select2.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('assets/plugin/bootstrap-timepicker.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') !!}" rel="stylesheet" />
    <link href="{!! asset('bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('bower_components/jquery.tagsinput/dist/jquery.tagsinput.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('bower_components/summernote/dist/summernote.css') !!}" rel="stylesheet" />
    <link href="{!! asset('bower_components/bootstrap-fileinput/css/fileinput.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('bower_components/datatables/media/css/dataTables.bootstrap.min.css') !!}" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- start: Highcharts CSS -->
    <link type="text/css" rel="stylesheet" id="skin_color" href="../bower_components/highcharts/highcharts.css" />

    <style>
        #printArea * {
            display: none;
            height: 100%;

        }

        @media print {
            @page {
                size: auto;
                margin: 0;
            }

            #nonPrintArea * {
                visibility: hidden;
            }

            #printArea * {
                /* display: unset; */
                display: block;
                height: 100%;
            }

            #printQR * {
                display: flex;
                justify-content: center;
                align-items: center;
                
            }

            #image2 {
                margin-top: 40%;
            }

            #wording {
                padding-top: 20px;
                text-align: center;
                font-size: 18px;
            }
        }
    </style>
    <style type="text/css" media="print">

    </style>
</head>

<body>
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <!-- start: RESPONSIVE MENU TOGGLER -->
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="clip-list-2"></span>
                </button>
                <!-- end: RESPONSIVE MENU TOGGLER -->
                <!-- start: LOGO -->
                <a class="navbar-brand" href="index.html">
                    <!-- CLIP<i class="clip-clip"></i>ONE -->
                    <div class="row">
                        <div class="col-sm-6" style="margin-top: -10px;">
                            <img src="{{asset('assets/images/logoz.png')}}" style="width: 165px;">
                        </div>
                    </div>
                </a>
                <!-- end: LOGO -->
            </div>
            <div class="navbar-tools">
                <!-- start: TOP NAVIGATION MENU -->
                <ul class="nav navbar-right">
                    <!-- start: USER DROPDOWN -->
                    <li class="dropdown current-user">
                        <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                            <img src="{{asset('assets/images/avatar-1-small.jpg')}}" class="circle-img" alt="">
                            <span class="username">{{auth()->user()->full_name}}</span>
                            <i class="clip-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                            <li>
                                <a href="{{route('logout')}}">
                                    <i class="clip-exit"></i> &nbsp;Log Out
                                </a>
                            </li>
                    </li>
                </ul>
                </li>
                <!-- end: USER DROPDOWN -->
                </ul>
                <!-- end: TOP NAVIGATION MENU -->
            </div>
        </div>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <ul class="main-navigation-menu">
                    <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
                        <!--active open-->
                        <a href="{{route('dashboard')}}">
                            <i class="clip-home-3"></i>
                            <span> Dasbor </span><span class="selected"></span>
                        </a>
                    </li>
                    <li class="{{ Request::is('penerbitan_dokumen/*') ? 'active open' : '' }}">
                        <a href="javascript:void(0)">
                            <i class="clip-file-plus"></i>
                            <span class="title"> Penerbitan Dokumen </span><i class="icon-arrow"></i>
                            <span class="selected"></span>
                        </a>
                        <ul class="sub-menu">
                            @if($sidemenu)
                                @foreach($sidemenu as $key => $value)
                                    <li class="{{ Request::is('penerbitan_dokumen/'.$value->id) || Request::is('penerbitan_dokumen/create/'.$value->id) || Request::is('penerbitan_dokumen/edit/'.$value->id.'_*') ? 'active open' : '' }}">
                                        <a href="{{ url('/penerbitan_dokumen/'.$value->id) }}">
                                            <span class="title"> {{ $value->judul }} </span>
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </li>
                    <li class="{{ Request::is('surat_masuk') || Request::is('surat_masuk_add') || Request::is('surat_masuk_edit/*') || Request::is('surat_masuk_detail/*') ? 'active open' : '' }}">
                        <a href="{{route('surat-masuk')}}">
                            <i class="fa fa-envelope-o"></i>
                            <span class="title"> Surat Masuk </span>
                        </a>
                    </li>
                    <li class="{{ Request::is('surat_keluar') || Request::is('surat_keluar_add') || Request::is('surat_keluar_edit/*') || Request::is('surat_keluar_detail/*') ? 'active open' : '' }}">
                        <a href="{{route('surat-keluar')}}">
                            <i class="fa fa-envelope"></i>
                            <span class="title"> Surat Keluar </span>
                        </a>
                    </li>
                    <li class="{{ Request::is('data_penduduk') || Request::is('data_penduduk_add') || Request::is('data_penduduk_edit/*') || Request::is('data_penduduk_detail/*') || Request::is('data_penduduk_import') || Request::is('data_penduduk/validasi_import') ? 'active open' : '' }}">
                        <a href="javascript:void(0)">
                            <i class="clip-data"></i>
                            <span class="title"> Basis Data </span><i class="icon-arrow"></i>
                            <span class="selected"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('data_penduduk') || Request::is('data_penduduk_add') || Request::is('data_penduduk_edit/*') || Request::is('data_penduduk_detail/*') || Request::is('data_penduduk_import') || Request::is('data_penduduk/validasi_import') ? 'active open' : '' }}">
                                <a href="{{route('penduduk')}}">
                                    <span class="title"> Data Penduduk </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @if(auth()->user()->role_id === 1 || auth()->user()->role_id === 2)
                    <li class="{{ Request::is('pengaturan_pengguna') || Request::is('pengaturan_pengguna_add') || Request::is('pengaturan_pengguna_edit/*') || Request::is('pengaturan_perihal') || Request::is('pengaturan_perihal_add') || Request::is('pengaturan_perihal_edit/*') || Request::is('pengaturan_penerbitan_surat') || Request::is('pengaturan_penerbitan_surat_add') || Request::is('pengaturan_penerbitan_surat_edit/*') || Request::is('pengaturan_penerbitan_surat_detail/*') || Request::is('pengaturan_terima_surat') || Request::is('pengaturan_terima_surat_add') || Request::is('pengaturan_terima_surat_edit/*') || Request::is('pengaturan_provdes') || Request::is('pengaturan_log') ? 'active open' : '' }}">
                        <a href="javascript:void(0)">
                            <i class="fa fa-gear"></i>
                            <span class="title"> Pengaturan </span><i class="icon-arrow"></i>
                            <span class="selected"></span>
                        </a>
                        <ul class="sub-menu">
                            @if(auth()->user()->role_id === 1)
                            <li class="{{ Request::is('pengaturan_provdes') ? 'active open' : '' }}">
                                <a href="{{route('provdes')}}">
                                    <span class="title"> Provinsi - Desa </span>
                                </a>
                            </li>
                            <li class="{{ Request::is('pengaturan_pengguna') || Request::is('pengaturan_pengguna_add') || Request::is('pengaturan_pengguna_edit/*') ? 'active open' : '' }}">
                                <a href="{{route('pengguna')}}">
                                    <span class="title"> Pengguna </span>
                                </a>
                            </li>
                            @endif
                            <li class="{{ Request::is('pengaturan_perihal') || Request::is('pengaturan_perihal_add') || Request::is('pengaturan_perihal_edit/*') ? 'active open' : '' }}">
                                <a href="{{route('perihal')}}">
                                    <span class="title"> Perihal </span>
                                </a>
                            </li>
                            <li class="{{ Request::is('pengaturan_penerbitan_surat') || Request::is('pengaturan_penerbitan_surat_add') || Request::is('pengaturan_penerbitan_surat_edit/*') || Request::is('pengaturan_penerbitan_surat_detail/*') ? 'active open' : '' }}">
                                <a href="{{route('penerbitan-surat')}}">
                                    <span class="title"> Penerbitan Surat </span>
                                </a>
                            </li>
                            <li class="{{ Request::is('pengaturan_terima_surat') || Request::is('pengaturan_terima_surat_add') || Request::is('pengaturan_terima_surat_edit/*') ? 'active open' : '' }}">
                                <a href="{{route('penerimaan-surat')}}">
                                    <span class="title"> Referensi Penerimaan Surat </span>
                                </a>
                            </li>
                            @if(auth()->user()->role_id === 1)
                            <li class="{{ Request::is('pengaturan_log') ? 'active open' : '' }}">
                                <a href="{{route('log')}}">
                                    <span class="title"> Log </span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                </ul>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
            <!-- start: PANEL CONFIGURATION MODAL FORM -->
            <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;
                            </button>
                            <h4 class="modal-title">Panel Configuration</h4>
                        </div>
                        <div class="modal-body">
                            Here will be a configuration form
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close
                            </button>
                            <button type="button" class="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- end: SPANEL CONFIGURATION MODAL FORM -->
            @yield('content')
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; Kabayan.
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <div id="event-management" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Event Management</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                        Close
                    </button>
                    <button type="button" class="btn btn-danger remove-event no-display">
                        <i class='fa fa-trash-o'></i> Delete Event
                    </button>
                    <button type='submit' class='btn btn-success save-event'>
                        <i class='fa fa-check'></i> Save
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="{{asset('bower_components/respond/dest/respond.min.js')}}"></script>
        <script src="{{asset('bower_components/Flot/excanvas.min.js')}}"></script>
        <script src="{{asset('bower_components/jquery-1.x/dist/jquery.min.js')}}"></script>
        <![endif]-->
    <script type="text/javascript" src="{!! asset('bower_components/jquery/dist/jquery.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('bower_components/sweetalert/dist/sweetalert.min.js') !!}"></script>


    <!--<![endif]-->
    <script type="text/javascript" src="{!! asset('bower_components/jquery-ui/jquery-ui.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('bower_components/bootstrap/dist/js/bootstrap.min.js') !!}"></script>

    <script type="text/javascript" src="{!! asset('bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('bower_components/blockUI/jquery.blockUI.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('bower_components/iCheck/icheck.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('bower_components/jquery.cookie/jquery.cookie.js') !!}"></script>

    <script type="text/javascript" src="{!! asset('assets/js/min/main.min.js') !!}"></script>
    <script src="{!! asset('bower_components/datatables/media/js/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('bower_components/datatables/media/js/dataTables.bootstrap.js') !!}"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="{!! asset('bower_components/moment/min/moment.min.js') !!}"></script>
    <script src="{!! asset('bower_components/bootstrap-maxlength/src/bootstrap-maxlength.js') !!}"></script>
    <script src="{!! asset('bower_components/autosize/dist/autosize.min.js') !!}"></script>
    <script src="{!! asset('bower_components/select2/dist/js/select2.min.js') !!}"></script>
    <script src="{!! asset('bower_components/select2/dist/js/i18n/id.js') !!}"></script>
    <script src="{!! asset('bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js') !!}"></script>
    <script src="{!! asset('bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js') !!}"></script>
    <script src="{!! asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}"></script>
    <script src="{!! asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min.js') !!}"></script>
    <script src="{!! asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') !!}"></script>
    <script src="{!! asset('bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js') !!}"></script>
    <script src="{!! asset('bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') !!}"></script>
    <script src="{!! asset('bower_components/jquery.tagsinput/src/jquery.tagsinput.js') !!}"></script>
    <script src="{!! asset('bower_components/summernote/dist/summernote.min.js') !!}"></script>
    <script src="{!! asset('bower_components/ckeditor/ckeditor.js') !!}"></script>
    <script src="{!! asset('bower_components/ckeditor/adapters/jquery.js') !!}"></script>
    <script src="{!! asset('bower_components/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js') !!}"></script>
    <script src="{!! asset('bower_components/bootstrap-fileinput/js/fileinput.min.js') !!}"></script>
    <!-- <script src="{!! asset('bower_components/jquery-validation/dist/jquery.validate.min.js') !!}"></script> -->
    <script src="{!! asset('bower_components/jquery-validation/dist/jquery.validate.js') !!}"></script>

    <script src="{!! asset('bower_components/jQuery-Smart-Wizard/js/jquery.smartWizard.js') !!}"></script>
    <script src="{!! asset('assets/js/min/form-wizard.min.js') !!}"></script>
    <script src="{!! asset('assets/js/form-elements.js') !!}"></script>
    <script src="{!! asset('assets/js/update.js') !!}"></script>
    <!-- <script src="{!! asset('assets/js/min/form-validation.min.js') !!}"></script> -->

    <script type="text/javascript" src="http://www.appelsiini.net/projects/chained/jquery.chained.js?v=0.9.10"></script>
    <script>
        jQuery(document).ready(function() {
            Main.init();
            FormElements.init();
            // FormWizard.init();
        });
    </script>
    <script>
        function myBlock() {
            var url = "{{asset('bower_components/jquery-colorbox/example4/loading.gif')}}";
            $.blockUI({
                message: '',
                css: {
                    backgroundColor: 'transparent',
                    border: '0'
                }

            });
        }

        function unBlock() {
            $.unblockUI();
        }
    </script>


    @stack('script')
</body>

</html>