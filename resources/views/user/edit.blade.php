@extends('layout/master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tambah User</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item">Data User</li>
                        <li class="breadcrumb-item active">Tambah User</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                        <div class="card-header">
                        <h3 class="card-title">Form User</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ url('user/update') }}" method="POST" id="form">
                            @csrf
                            <input type="hidden" id="id" name="id" value="{{ $user->id }}">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="">Nama</label>
                                    <input type="text" name="full_name" class="form-control" id="full_name" value="{{ $user->full_name }}" placeholder="Nama lengkap anda">
                                </div>
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="email" name="email" class="form-control" id="email" value="{{ $user->email }}" placeholder="Masukkan email anda">
                                </div>
                                <div class="form-group">
                                    <label for="">Username</label>
                                    <input type="text" name="username" class="form-control" id="username" value="{{ $user->username }}" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <label for="">Role</label>
                                    <select name="role" id="role" class="form-control select2bs4" style="width: 100%;">
                                        <option value="">-- Silakan pilih role user --</option>
                                        @foreach ($role as $data)
                                            <option value="{{ $data->id }}" {{ $user->role_id == $data->id ? 'selected' : '' }}>{{ $data->role_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Tanggal Bergabung</label>
                                    <input type="text" name="join_date" class="form-control datepicker tanggal-readonly" id="join_date" value="{{ $user->join_date }}" readonly>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class="float-right">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <a href="{{ route('user') }}" class="btn btn-danger">Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
                <!-- /.card -->
                </div>
                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-6">

                </div>
                <!--/.col (right) -->
            </div>
        <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
@push('script')
<script>
    var tipe_form = 'edit';
</script>
<script type="text/javascript" src="{{ asset('js/custom/user.js') }}"></script>
@endpush
