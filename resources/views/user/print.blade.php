<html>
<head>
	<title>
		@if($type == 0)
			Daftar Semua User
		@else
			User {{ $users->full_name }}
		@endif
	</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	{{-- <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}"> --}}
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}

		.invisible {
			opacity: 0px !important;
		}

		.paragraph-justify{
			text-align: justify !important;
			text-justify: inter-word !important;
		}

		.font-style-surat-isi{
			font-family: "Times New Roman", Times, serif !important;
			font-size: 15px !important;
		}

		.font-style-surat-judul{
			font-family: "Times New Roman", Times, serif !important;
			font-size: 18px !important;
		}

		.mt-5{
			margin-top: 10px !important;
		}

		.text-align-center{
			text-align: center !important;
		}

		.on-middle{
			margin: 0px auto;
		}

		.margin-table{
			margin-left: 50px !important;
			margin-bottom: 20px !important;
		}

		.margin-page{
			margin-left: 50px !important;
			margin-right: 20px !important;
		}

		/** Define the footer rules **/
        footer {
            position: fixed; 
            bottom: 0cm; 
            left: 0cm; 
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            /*background-color: #03a9f4;
            color: white;
            text-align: center;
            line-height: 1.5cm;*/
        }
	</style>
</head>
<body class="margin-page">
	<center>
		<h5 class="font-style-surat-judul">
			<u>
				@if($type == 0)
					Daftar Semua User
				@else	
					User {{ $users->full_name }}
				@endif
			</u>
		</h5>
	</center>
 	<br>
 	@if($type == 0)
	 	<table class="margin-table" width="100%" border="1">
		 	<thead>
		 		<tr>
		 			<th>No.</th>
		 			<th>Nama</th>
		 			<th>Username</th>
		 			<th>Email</th>
		 			<th>Role</th>
		 			<th>Tanggal Bergabung</th>
		 		</tr>
		 	</thead>
		 	<tbody>
			 	@if($users)
					@foreach($users as $key => $value)
						<tr>
				 			<td class="text-center">{{ $key + 1 }}.</td>
				 			<td>{{ $value->full_name }}</td>
				 			<td>{{ $value->username }}</td>
				 			<td>{{ $value->email }}</td>
				 			<td>{{ $value->role_name }}</td>
				 			<td>{{ date('d-M-Y', strtotime($value->join_date)) }}</td>
				 		</tr>
					@endforeach
			 	@else
			 		<tr>
			 			<td colspan="4">Tidak ada data.</td>
			 		</tr>
			 	@endif
		 	</tbody>
		</table>
	@else
		<table class="margin-table" width="100%" border="1">
		 	<tbody>
			 	<tr>
		 			<td>Nama</td>
		 			<td class="text-center">:</td>
		 			<td>{{ $users->full_name }}</td>
		 		</tr>
		 		<tr>
		 			<td>Username</td>
		 			<td class="text-center">:</td>
		 			<td>{{ $users->username }}</td>
		 		</tr>
		 		<tr>
		 			<td>Email</td>
		 			<td class="text-center">:</td>
		 			<td>{{ $users->email }}</td>
		 		</tr>
		 		<tr>
		 			<td>Role</td>
		 			<td class="text-center">:</td>
		 			<td>{{ $users->role_name }}</td>
		 		</tr>
		 		<tr>
		 			<td>Tanggal Bergabung</td>
		 			<td class="text-center">:</td>
		 			<td>{{ date('d-M-Y', strtotime($users->join_date)) }}</td>
		 		</tr>
		 	</tbody>
		</table>
	@endif
</body>
</html>