@extends('layout/master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Resep</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Resep</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            @include('include.alert')
            <div class="card">
              <div class="card-header">
                <div class="float-right">
                    <a href="{{ url('resep/add') }}" class="btn btn-primary"> Tambah </a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Tanggal</th>
                    <th>Nama Pasien</th>
                    <th>Total Stok</th>
                    <th width="10%">Pilihan</th>
                  </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
@push('script')
  <script>
      function initDatatable() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('resep/dtJson') }}",
            "aoColumnDefs": [{
                    "sWidth": "5%",
                    "aTargets": [0],
                    "className": "text-center"
                }
            ],
            "language": {
              "emptyTable": "Tidak Ada Data",
              "zeroRecords": "Data Tidak Ditemukan"
            },
            "autoWidth": false,
            "responsive": true,
            columns: [
                  {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'tanggal',
                    name: 'tanggal'
                },
                  {
                    data: 'nama_pasien',
                    name: 'nama_pasien'
                },
                {
                    data: 'total_stok',
                    name: 'total_stok'
                },
                  {
                    data: 'action',
                    name: 'action',
                    className: 'text-center'
                },
            ]
        });
      }

    $(document).ready(function () {
      initDatatable();
    });
  </script>
@endpush
