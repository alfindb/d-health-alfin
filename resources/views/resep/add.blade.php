@extends('layout/master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tambah Resep</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/resep') }}">Data Resep</a></li>
                        <li class="breadcrumb-item active">Tambah Resep</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                        <div class="card-header">
                        <h3 class="card-title">Form Resep</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ url('resep/save') }}" method="POST" id="form">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Nama Pasien</label>
                                            <input type="text" class="form-control" name="nama_pasien" id="nama_pasien" required>   
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Tipe Obat</label>
                                            <select name="tipe_obat" class="form-control" id="tipe_obat">
                                                <option value="">-- Pilih Tipe Obat --</option>
                                                <option value="1">Non Racikan</option>
                                                <option value="2">Racikan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="text-center">
                                            <h5><b>Pilih Obat</b> (Silakan Pilih Tipe Terlebih Dahulu)</h5>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div id="div_input">
                                        
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4><b>Daftar Obat</b></h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="table" class="table">
                                            <thead>
                                                <tr>
                                                    <th>Tipe</th>
                                                    <th>Obat</th>
                                                    <th>Quantity</th>
                                                    <th>Aturan Pakai</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="table_body">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="float-right">
                                            <b>Total Stok : <span id="total_stok_text">0</span></b>
                                            <input type="hidden" name="total_stok" id="total_stok" value="0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class="float-right">
                                    <button type="button" class="btn btn-primary" id="button_submit">Simpan</button>
                                    <a href="{{ url('/resep') }}" class="btn btn-danger">Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
                <!-- /.card -->
                </div>
                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-6">

                </div>
                <!--/.col (right) -->
            </div>
        <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
{{-- <input type="hidden" id="list_obat" value="{{ json_encode($obat) }}"> --}}

@endsection
@push('script')
<script>
    let row = 0;
    let total_stok = 0;

    function initSelect2(){
        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
    }

    function onChangeTipeObat(){
        $('#tipe_obat').on('change', function () {
            const tipe = $(this).find(':selected').val();

            if(tipe === ''){
                $('#div_input').html('');
            }else{
                $.ajax({
                    type: "GET",
                    url: "{{ url('resep/get_by_type') }}",
                    data: {
                        tipe: tipe
                    },
                    success: function (res) {
                        if(res.status === true){
                            const data_signa = res.data.signa;
                            let option_signa = ``;
                            for (let i = 0; i < data_signa.length; i++) {
                                option_signa += `
                                    <option value="`+data_signa[i].signa_id+`" data-nama="`+data_signa[i].signa_nama+`">`+data_signa[i].signa_nama+`</option>
                                `;
                            }


                            let html =  ``;
                            
                            if(tipe === '1'){
                                
                                const data_obat = res.data.obat;
                                
                                let option = ``;
                                for (let i = 0; i < data_obat.length; i++) {
                                    option += `
                                        <option value="`+data_obat[i].obatalkes_id+`" data-nama="`+data_obat[i].obatalkes_nama+`">`+data_obat[i].obatalkes_nama+`</option>
                                    `;
                                }

                                html += `
                                    <div class="row form-group">
                                        <div class="col-md-3">
                                            <label for="">Obat</label>
                                            <select class="form-control select2bs4" id="obat">
                                                <option value="">-- Pilih Obat --</option>
                                                `+option+`
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">Stok Tersedia</label>
                                            <input type="number" class="form-control" name="stok" id="stok" value="" readonly>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">Quantity</label>
                                            <input type="number" class="form-control" name="qty" id="qty" value="">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">Signa</label>
                                            <select class="form-control select2bs4" id="signa">
                                                <option value="">-- Pilih Signa --</option>
                                                `+option_signa+`
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="float-right">
                                                <button type="button" class="btn btn-success" id="add_button_non_racikan">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                `;
                                $('#div_input').html(html);
                                onChangeObat();
                                onClickAddObat();

                            }else if(tipe === '2'){

                                const data_racikan = res.data.racikan;
                                
                                let option = ``;
                                for (let i = 0; i < data_racikan.length; i++) {
                                    option += `
                                        <option value="`+data_racikan[i].id+`" data-nama="`+data_racikan[i].nama+`">`+data_racikan[i].nama+`</option>
                                    `;
                                }

                                html += `
                                    <div class="row form-group">
                                        <div class="col-md-3">
                                            <label for="">Racikan</label>
                                            <select class="form-control select2bs4" id="racikan">
                                                <option value="">-- Pilih Racikan --</option>
                                                `+option+`
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">Quantity</label>
                                            <input type="number" class="form-control" id="qty" name="" value="">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">Signa</label>
                                            <select class="form-control select2bs4" id="signa">
                                                <option value="">-- Pilih Signa --</option>
                                                `+option_signa+`
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text-center">
                                                <h5><b>Detail Bahan</b> (Silakan Pilih Tipe Terlebih Dahulu)</h5>
                                            </div> 
                                        </div>
                                    </div>
                                    <hr>
                                    <div id="div_detail_bahan">

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="float-right">
                                                <button type="button" class="btn btn-success" id="add_button_racikan">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                `;
                                $('#div_input').html(html);
                                onChangeRacikan();
                            }

                            initSelect2();
                        }
                    }
                });
            }
        });
    }

    function onChangeObat(){
        $('#obat').on('change', function () {
            const id = $(this).find(':selected').val();
            
            if(id === ''){
                $('#stok').val('');
                $('#qty').val('');
            }else{
                $.ajax({
                    type: "GET",
                    url: "{{ url('/resep/get_stok_obat') }}",
                    data: {
                        id: id
                    },
                    success: function (res) {
                        if(res.status === true){
                            $('#stok').val(res.data);
                        }
                    }
                });
            }

        });
    }

    function onClickAddObat(){
        $('#add_button_non_racikan').on('click', function () {
            const obat_id = $('#obat').find(':selected').val();
            const obat_nama = $('#obat').find(':selected').data('nama');
            const stok = parseFloat( $('#stok').val() );
            const qty = parseFloat( $('#qty').val() );
            const signa_id = $('#signa').find(':selected').val();
            const signa_nama = $('#signa').find(':selected').data('nama');

            /* validasi */
            if(obat_id === '' || qty === '' || signa_id === ''){
                alert('Pastikan Semua Kolom Terisi');
            }else{
                /* vaidasi stok */
                if(qty > stok){
                    alert('Stok tidak tersedia');
                }else{
                    $.ajax({
                        type: "POST",
                        url: "{{ url('/resep/add_obat') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            obat_id : obat_id,
                            qty : qty
                        },
                        success: function (res) {
                            if(res.status === true){
                                let array_obat = [];
                                let object_obat = {
                                    tipe: 1,
                                    obat_id: obat_id,
                                    obat_nama: obat_nama,
                                    qty: qty,
                                    signa_id: signa_id,
                                    signa_nama: signa_nama,
                                }
                                array_obat.push(object_obat)

                                let html = `
                                    <tr id="tr_`+row+`">
                                        <td>
                                            Non Racikan
                                        </td>
                                        <td>
                                            `+obat_nama+`
                                        </td>
                                        <td>
                                            `+qty+`
                                        </td>
                                        <td>
                                            `+signa_nama+`
                                        </td>
                                        <td>
                                            <input type="hidden" name="detail[]" value='`+JSON.stringify(array_obat)+`'>
                                            <button type="button" class="btn btn-danger btn-sm delete-list-obat" data-row="`+row+`" data-data="`+res.data+`" onClick="onClickDeleteListObat(`+res.data+`, `+row+`)" ><i class="fas fa-trash"></i></button>
                                        </td>
                                    </tr>
                                `;
                                
                                $('#table_body').append(html);

                                // onClickDeleteListObat();
                                row++;
                                modifyTotalStok(1, qty);

                                $('#tipe_obat').val('').trigger('change');
                            }else{
                                alert('Terjadi Kesalahan Pada Server')
                            }
                        }
                    });
                }
            }

        });
    }

    function onClickDeleteListObat(data, row){
        $.ajax({
            type: "POST",
            url: "{{ url('/resep/delete_obat') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                id: data
            },
            success: function (res) {
                if(res.status === true){
                    $('#tr_'+row).remove();
                    modifyTotalStok(0, parseFloat(res.data));
                }else{
                    alert('Terjadi Kesalahan Pada Server');
                }
            }
        });
    }

    function onChangeRacikan(){
        $('#racikan').on('change', function () {
            const id = $(this).find(':selected').val();
            
            if(id === ''){
                $('#div_detail_bahan').html('')
            }else{
                $.ajax({
                    type: "GET",
                    url: "{{ url('/resep/get_stok_racikan') }}",
                    data: {
                        id: id
                    },
                    success: function (res) {
                        if(res.status === true){
                            let detail_html = ``;
                            for (let i = 0; i < res.data.length; i++) {
                                detail_html += `
                                    <div class="row form-group">
                                        <div class="col-md-3">
                                            <label for="">Obat</label>
                                            <input type="hidden" id="obatalkes_id_`+i+`" value="`+res.data[i].obatalkes_id+`">
                                            <input type="text" class="form-control" id="obatalkes_nama_`+i+`" name="obatalkes_nama[]" value="`+res.data[i].obatalkes_nama+`" readonly>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">Stok</label>
                                            <input type="text" class="form-control stok-detail" id="stok_detail_`+i+`" name="obatalkes_stok[]" value="`+res.data[i].stok_live+`" readonly>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">Quantity</label>
                                            <input type="text" class="form-control qty-detail" id="qty_detail_`+i+`" value="`+res.data[i].qty+`" readonly>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">Final Quantity</label>
                                            <input type="text" class="form-control final-qty-detail" id="final_qty_detail_`+i+`" data-key="`+i+`" name="final_quantity[]" value="" readonly>
                                        </div>
                                    </div>
                                `;
                            }

                            $('#div_detail_bahan').html(detail_html)
                            onKeyUpQty();
                            onClickAddRacikan();
                        }
                    }
                });
            }
        });
    }

    function onKeyUpQty(){
        $('#qty').on('keyup', function () {
            const qty = $(this).val();

            if(qty === ''){
                $('.final-qty-detail').each(function (index, element) {
                    $(this).val('')
                });
            }else{
                $('.final-qty-detail').each(function (index, element) {
                    const key = $(this).data('key')
                    const qty_detail = $('#qty_detail_'+key).val();

                    let final_qty = qty * qty_detail;
                    $(this).val(final_qty)
                });
            }
        });
    }

    function onClickAddRacikan(){
        $('#add_button_racikan').on('click', function () {
            const racikan_id = $('#racikan').find(':selected').val();
            const racikan_nama = $('#racikan').find(':selected').data('nama');
            const qty = parseFloat( $('#qty').val() );
            const signa_id = $('#signa').find(':selected').val();
            const signa_nama = $('#signa').find(':selected').data('nama');

            /* validasi */
            if(racikan_id === '' || qty === '' || signa_id === ''){
                alert('Pastikan Semua Kolom Terisi');
            }else{
                let all_stok_avail = 1;
                let array_racikan_input = [];
                let detail_racikan = ``;
                $('.final-qty-detail').each(function (index, element) {
                    const key = $(this).data('key');
                    const obatalkes_id = $('#obatalkes_id_'+key).val();
                    const obatalkes_nama = $('#obatalkes_nama_'+key).val();
                    const final_qty = parseFloat( $(this).val() );
                    const stok_detail = parseFloat( $('#stok_detail_'+key).val() );

                    if(final_qty > stok_detail){
                        all_stok_avail  = 0;
                    }

                    /* object */
                    let object_racikan_input = {
                        obatalkes_id: obatalkes_id,
                        obatalkes_nama: obatalkes_nama,
                        qty: final_qty
                    }

                    array_racikan_input.push(object_racikan_input);

                    detail_racikan += `
                        - `+obatalkes_nama+`, qty : `+final_qty+`<br>
                    `;
                });

                /* vaidasi stok */
                if(all_stok_avail === 0){
                    alert('Stok tidak tersedia');
                }else{
                    $.ajax({
                        type: "POST",
                        url: "{{ url('/resep/add_racikan') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            array_racikan_input: array_racikan_input
                        },
                        success: function (res) {
                            if(res.status === true){
                                console.log(res.data);
                                let array_racikan = [];
                                let object_racikan = {
                                    tipe: 2,
                                    racikan_id: racikan_id,
                                    racikan_nama: racikan_nama,
                                    qty: qty,
                                    signa_id: signa_id,
                                    signa_nama: signa_nama,
                                    detail: array_racikan_input
                                }
                                array_racikan.push(object_racikan)

                                let html = `
                                    <tr id="tr_`+row+`">
                                        <td>
                                            Racikan
                                        </td>
                                        <td>
                                            `+racikan_nama+`<br>
                                            `+detail_racikan+`
                                        </td>
                                        <td>
                                            `+qty+`
                                        </td>
                                        <td>
                                            `+signa_nama+`
                                        </td>
                                        <td>
                                            <input type="hidden" name="detail[]" value='`+JSON.stringify(array_racikan)+`'>
                                            <button type="button" class="btn btn-danger btn-sm delete-list-racikan" data-row="`+row+`" data-data='`+JSON.stringify(res.data)+`' onClick="onClickDeleteListRacikan(`+JSON.stringify(res.data)+`, `+row+`,`+qty+`)" ><i class="fas fa-trash"></i></button>
                                        </td>
                                    </tr>
                                `;
                                
                                $('#table_body').append(html);

                                // onClickDeleteListObat();
                                row++;
                                modifyTotalStok(1, qty);

                                $('#tipe_obat').val('').trigger('change');
                            }else{
                                alert('Terjadi Kesalahan Pada Server')
                            }
                        }
                    });
                }
            }
        });
    }

    function onClickDeleteListRacikan(data, row, qty){

        $.ajax({
            type: "POST",
            url: "{{ url('/resep/delete_racikan') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                id: data
            },
            success: function (res) {
                if(res.status === true){
                    $('#tr_'+row).remove();
                    modifyTotalStok(0, parseFloat(qty));
                }else{
                    alert('Terjadi Kesalahan Pada Server');
                }
            }
        });
    }

    function modifyTotalStok(is_add, qty){
        if(is_add === 1){
            total_stok += qty;
        }else{
            total_stok -= qty;
        }

        $('#total_stok_text').text(total_stok);
        $('#total_stok').val(total_stok)
    }

    function onSubmit(){
        $('#button_submit').on('click', function () {
            const nama_pasien = $('#nama_pasien').val();
            const div = $('#table_body tr').length;

            if(nama_pasien === ''){
                alert('Silakan Input Nama Pasien Terlebih Dahulu')
            }else{
                if(div < 1){
                    alert('Silakan Masukkan Obat Terlebih Dahulu')
                }else{
                    $('#form').submit();
                }
            }
        });
    }

    $(document).ready(function () {
        onChangeTipeObat();
        onSubmit();
    });
</script>
@endpush
