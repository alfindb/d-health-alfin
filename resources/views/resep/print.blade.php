<html>
<head>
	<title>
		Daftar Resep
	</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	{{-- <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}"> --}}
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}

		.invisible {
			opacity: 0px !important;
		}

		.paragraph-justify{
			text-align: justify !important;
			text-justify: inter-word !important;
		}

		.font-style-surat-isi{
			font-family: "Times New Roman", Times, serif !important;
			font-size: 15px !important;
		}

		.font-style-surat-judul{
			font-family: "Times New Roman", Times, serif !important;
			font-size: 18px !important;
		}

		.mt-5{
			margin-top: 10px !important;
		}

		.text-align-center{
			text-align: center !important;
		}

		.on-middle{
			margin: 0px auto;
		}

		.margin-table{
			margin-left: 50px !important;
			margin-bottom: 20px !important;
		}

		.margin-page{
			margin-left: 50px !important;
			margin-right: 20px !important;
		}

		/** Define the footer rules **/
        footer {
            position: fixed; 
            bottom: 0cm; 
            left: 0cm; 
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            /*background-color: #03a9f4;
            color: white;
            text-align: center;
            line-height: 1.5cm;*/
        }
	</style>
</head>
<body class="margin-page">
	<center>
		<table width="100%">
			<tr>
				<td width="20%">Nama Pasien</td>
				<td width="5%">:</td>
				<td>{{ $resep->nama_pasien }}</td>
			</tr>
			<tr>
				<td>Tanggal</td>
				<td>:</td>
				<td>{{ date('d-m-Y', strtotime($resep->tanggal)) }}</td>
			</tr>
		</table>
		<h5 class="font-style-surat-judul">
			<u>
				Daftar Resep
			</u>
		</h5>
	</center>
 	<br>
	 <table class="margin-table" width="100%" border="1">
		<thead>
			<tr>
				<th>Tipe</th>
				<th>Obat</th>
				<th>Quantity</th>
				<th>Aturan Pakai</th>
			</tr>
		</thead>
		<tbody id="table_body">
			@if($resep_detail)
				@foreach ($resep_detail as $key => $val)
					@if ($val->tipe == 1)
						<tr>
							<td>
								Non Racikan
							</td>
							<td>
								{{ $val->obat_nama }}
							</td>
							<td>
								{{ number_format($val->qty) }}
							</td>
							<td>
								{{ $val->signa_nama }}
							</td>
						</tr>
					@elseif($val->tipe == 2)
						<tr>
							<td>
								Racikan
							</td>
							<td>
								{{ $val->racikan_nama }}<br>
								@if ($val->detail)
									@foreach ($val->detail as $item)
										- {{ $item->obatalkes_nama }}, qty : {{ number_format($item->qty) }}<br>
									@endforeach
								@endif
							</td>
							<td>
								{{ number_format($val->qty) }}
							</td>
							<td>
								{{ $val->signa_nama }}
							</td>
						</tr>
					@endif
				@endforeach
			@endif
		</tbody>
	</table>
</body>
</html>