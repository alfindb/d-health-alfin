@extends('layout/master')
@section('content')
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: PAGE TITLE & BREADCRUMB -->
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-gear"></i>
                    <a href="#"> Pengaturan </a>
                </li>
                <li class="active">
                    Log
                </li>
            </ol>
            <div class="page-header">
                <h1>Daftar Log</h1>
            </div>
            <!-- end: PAGE TITLE & BREADCRUMB -->
        </div>
    </div>
    <!-- end: PAGE HEADER -->
    <!-- start: PAGE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <!-- start: DYNAMIC TABLE PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> Tabel Daftar Log
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#"> </a>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover table-full-width" id="logs-table">
                        <thead>
                            <tr>
                                <th class="center">No</th>
                                <th>User</th>
                                <th>Aksi</th>
                                <td>Deskripsi</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end: DYNAMIC TABLE PANEL -->
        </div>
    </div>
    <!-- end: PAGE CONTENT-->
</div>
<script src="//code.jquery.com/jquery.js"></script>
<script>
    $(function() {
        $('#logs-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{url("pengaturan_log/json")}}',
            "aoColumnDefs": [{
                    "sWidth": "5%",
                    "aTargets": [0],
                    "className": "text-center"
                }

            ],
            "language": {
                "emptyTable": "Tidak Ada Data",
                "zeroRecords": "Data Tidak Ditemukan"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'user',
                    name: 'user'
                },
                {
                    data: 'action',
                    name: 'action'
                },
                {
                    data: 'description',
                    name: 'description'
                }
            ]
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('body').on('click', '.delete', function() {

            var delete_url = $(this).attr('data-url');

            swal({
                title: "Apakah Anda Yakin Akan Menghapus Data Perihal ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Hapus !",
                cancelButtonText: "Batalkan",
                closeOnConfirm: false
            }, function() {

                window.location.href = delete_url;
            });

            return false;
        });
    });
</script>
@if(session('success'))
<script>
    $(document).ready(function() {
        swal("Berhasil!", "{{session('success')}}", "success");
    });
</script>
@endif
@endsection