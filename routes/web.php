<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AuthController@index');
Route::post('/login', "AuthController@login")->name('login');
Route::get('/logout', "AuthController@logout")->name('logout');

// Admin, Input Data & Verifikator
Route::group(['middleware' => ['auth']], function () {
    // Dashboard
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/dashboard/countUsersByYear', 'DashboardController@countUsersByYear')->name('dashboard.countUsersByYear');

    // User
    // Route::get('/user/json', 'UserController@json');
    // Route::get('/user', 'UserController@index')->name('user');
    // Route::get('/user/add', 'UserController@add');
    // Route::post('/user/save', 'UserController@save');
    // Route::get('/user/detail/{id}', 'UserController@detail');
    // Route::get('/user/edit/{id}', 'UserController@edit');
    // Route::post('/user/update', 'UserController@update');
    // Route::get('/user/checkIsExist/{type}', 'UserController@checkIsExist');
    // Route::get('/user/checkIsExist/{type}/{id}', 'UserController@checkIsExist');
    // Route::get('/user/print/{id}', 'UserController@print');

    // Non Racikan
    Route::get('/non_racikan', 'NonRacikanController@index')->name('non_racikan');
    Route::get('/non_racikan/dtJson', 'NonRacikanController@dtJson');

    // Racikan
    Route::get('/racikan', 'RacikanController@index')->name('racikan');
    Route::get('/racikan/dtJson', 'RacikanController@dtJson');
    Route::get('/racikan/add', 'RacikanController@add');
    Route::post('/racikan/save', 'RacikanController@save');
    Route::get('/racikan/detail/{id}', 'RacikanController@detail');
    Route::get('/racikan/edit/{id}', 'RacikanController@edit');
    Route::post('/racikan/update', 'RacikanController@update');

    // Resep
    Route::get('/resep', 'ResepController@index')->name('resep');
    Route::get('/resep/dtJson', 'ResepController@dtJson');
    Route::get('/resep/add', 'ResepController@add');
    Route::post('/resep/save', 'ResepController@save');
    Route::get('/resep/detail/{id}', 'ResepController@detail');
    Route::get('/resep/print/{id}', 'ResepController@print');
    Route::get('/resep/get_by_type', 'ResepController@getByType');
    Route::get('/resep/get_stok_obat', 'ResepController@getStokObat');
    Route::get('/resep/get_stok_racikan', 'ResepController@getStokRacikan');
    Route::post('/resep/add_obat', 'ResepController@addObat');
    Route::post('/resep/delete_obat', 'ResepController@deleteObat');
    Route::post('/resep/add_racikan', 'ResepController@addRacikan');
    Route::post('/resep/delete_racikan', 'ResepController@deleteRacikan');
});
